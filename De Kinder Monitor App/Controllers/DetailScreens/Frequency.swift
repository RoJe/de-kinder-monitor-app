//
//  NewHeartBeat.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 1/22/20.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

class Frequency: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    /// The minutes allowed for the garph to search
    private let graphTimeDate = [61, 180, 1440, 4320]
    
    /// The allowed time formats
    private let dateFormats = ["HH:mm:ss", "HH:mm:ss", "HH:mm", "dd HH", "HH:mm:ss"]
    
    /// The data inside the picker
    var pickerData: [String] = [String]()
    
    /// What picker is active
    var activePicker: Int = 0
    
    /// Current active minutes
    private var minutes: Int = 61
    
    
    /// The picker
    @IBOutlet weak var uiPicker: UIPickerView!
    
    /// The graph
    @IBOutlet weak var LineChart: LineChartTests!
    
    /// Label with the value of the sensor
    @IBOutlet weak var sensorValue: UILabel!
    
    /// Sensor status icon
    @IBOutlet weak var sensorStatusIcon: UIImageView!
    
    
    /// Spin my.. oh god kill me
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// Times for the graph
    @IBOutlet weak var graphTime: UISegmentedControl!
    
    
    /// Min picker button
    @IBOutlet weak var minPicker: PickerButton!
    
    /// Max picker button
    @IBOutlet weak var maxPicker: PickerButton!
    
    /// Time picker button
    @IBOutlet weak var alarmPicker: PickerButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.isHidden = false;
        self.SetPlaceHolders()
        self.CheckPermissionFunctionallity()
        self.update()
        self.CreateObserver()
        self.GefreshPickerData()
        
        uiPicker.dataSource = self;
        uiPicker.delegate = self;
    }
    
    /// Check if the user is allowed to update the limits
    private func CheckPermissionFunctionallity(){
        if !(User.GetPermissionData().write ?? false) {
            minPicker.setImage(nil, for: .normal)
            minPicker.layer.borderWidth = 0;
            maxPicker.setImage(nil, for: .normal)
            maxPicker.layer.borderWidth = 0;
        }
    }
    
    /// Function for when the graph time button has been pressed
    /// - Parameter sender: the button
    @IBAction func graphTimeChanged(_ sender: Any) {
        self.activityIndicator.isHidden = false;
        let time = self.graphTimeDate[graphTime.selectedSegmentIndex]
        self.minutes = time
        self.GefreshPickerData()
    }
    
    /// Function for when the min picker button has been pressed
    /// - Parameter sender: the button
    @IBAction func minPickerPressed(_ sender: Any) {
        if !(User.GetPermissionData().write ?? false) {
            return
        }
        activePicker = 0;
        uiPicker.isHidden = false;
        pickerData = GetRangeValues(true);
        self.uiPicker.reloadAllComponents()
        
    }
    
    // Function for when the max picker button has been pressed
    /// - Parameter sender: the button
    @IBAction func maxPickerPressed(_ sender: Any) {
        if !(User.GetPermissionData().write ?? false) {
            return
        }
        activePicker = 1;
        uiPicker.isHidden = false;
        pickerData = GetRangeValues(false);
        self.uiPicker.reloadAllComponents()
    }
    
    // Function for when the alarm picker button has been pressed
    /// - Parameter sender: the button
    @IBAction func alarmPickerPressed(_ sender: Any) {
        activePicker = 2;
        uiPicker.isHidden = false;
        pickerData = ["1m", "5m", "10m", "15m", "20m"];
        uiPicker.reloadAllComponents()
    }
    
    /// Get the number of items inside the picker
    /// - Parameter pickerView: the picker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
         // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int)-> Int {
        return pickerData.count
    }
        
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    /// Function to update the sensor stuff
    func update() {
        
        let sensorText = Sensor.GetFrequancy() + " Xpm";
        if(!Sensor.IsWithinFrequancyBounds()) {
            sensorStatusIcon.tintColor = Colors.Red
            sensorValue.textColor = Colors.Red;
            sensorValue.text = sensorText;
            
            return
        }
        sensorStatusIcon.tintColor = Colors.Green
        sensorValue.textColor = Colors.Green;
        
        sensorValue.text = sensorText;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let textData = pickerData[row];
        switch(activePicker) {
            case 0:
                minPicker.setTitle(String(pickerData[row]), for: UIControl.State.normal)
                Sensor.SetFrequancyLimits(min: Int(textData)!, max: Sensor.GetFrequancyLimits().max)
                uiPicker.isHidden = true
                break;
            case 1:
                maxPicker.setTitle(String(pickerData[row]), for: UIControl.State.normal)
                Sensor.SetFrequancyLimits(min: Sensor.GetFrequancyLimits().min, max: Int(textData)!)
                uiPicker.isHidden = true
                break;
            case 2:
                var data = row * 5 * 60;
                if(row == 0) {
                    data = 60
                }

                Sensor.SetFrequencySnooze(timeToAdd: Double(data))
                alarmPicker.setTitle(String(pickerData[row]), for: UIControl.State.normal)
                uiPicker.isHidden = true
                break;
            default:
                break;
        }
    }
    
    /// Function to set the placeholders
    private func SetPlaceHolders(){
       let limits = Sensor.GetFrequancyLimits();
       minPicker.setTitle(String(limits.min), for: UIControl.State.normal)
       maxPicker.setTitle(String(limits.max), for: UIControl.State.normal)
       alarmPicker.setTitle(String( Int(Sensor.GetFrequencySnooze() / 60)) + "m", for: UIControl.State.normal)
   }
    
    /// Get the range values for the picker
    /// - Parameter type: which direction
    private func GetRangeValues(_ type: Bool) -> [String]{
        if(type) {
            var values = [String]()
            let fallMin = Sensor.getFrequancyFallback().min
            let limitMax = Sensor.GetFrequancyLimits().max;
            for i in fallMin...limitMax {
                values.append(String(i))
            }
            return values
        } else {
            var values = [String]()
            let limitMin = Sensor.GetFrequancyLimits().min;
            let fallMax = Sensor.getFrequancyFallback().max
            
            for i in limitMin...fallMax {
                values.append(String(i))
            }
            return values;
        }
   }
   
    /// MEMEORY
   override func didReceiveMemoryWarning() {
       super.didReceiveMemoryWarning()
   }
    
    /// Get the graph data from the api
    private func GefreshPickerData(){
        Network.GetConnectedPatients(urlFail: {
            Network.netAlert()
        }, comp: { (json, error, response) in
            if let json = json {
                do {
                    let result = try JSONDecoder().decode([PatientData].self, from: Data(json.utf8) )
                    
                    let currDate = Date()
                    Network.GetSensorData(
                        name: "Freq",
                        patientId: result.first?.patientID ?? 0,
                        from: currDate.adding(minutes: -self.minutes),
                        to: currDate,
                        urlFail: {
                        }, comp: { (json, error, response) in
                            do {
                                if let json = json {
                                    let dateFormatterGet = DateFormatter()
                                    dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"

                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = self.dateFormats[self.graphTime.selectedSegmentIndex]

                                    let pickerData = try JSONDecoder().decode(GraphPointSimple.self, from: Data(json.utf8) )
                                    self.activityIndicator.isHidden = true;
                                    let dateObjects = (pickerData.time.compactMap { dateFormatterGet.date(from: $0) }).compactMap { dateFormatter.string(from: $0) }
                                    self.LineChart.setChart(dataPoints: dateObjects, values: pickerData.value);
                                }
                               
                            }catch{
                                CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""),
                                msg: NSLocalizedString("ErrorGraph", comment: ""),
                                h1Text: NSLocalizedString("Close", comment: ""),
                                h1: {(alert: UIAlertAction!) in
                                    Navigation.pop();
                                });
                                self.activityIndicator.isHidden = true;
                            }
                            
                        }, fail:{ error in
                            CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""),
                            msg: NSLocalizedString("ErrorGraph", comment: ""),
                            h1Text: NSLocalizedString("Close", comment: ""),
                            h1: {(alert: UIAlertAction!) in
                                Navigation.pop();
                            });
                            self.activityIndicator.isHidden = true;
                    })
                }catch{
                    CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""),
                    msg: NSLocalizedString("ErrorGraph", comment: ""),
                    h1Text: NSLocalizedString("Close", comment: ""),
                    h1: {(alert: UIAlertAction!) in
                        Navigation.pop();
                    });
                    self.activityIndicator.isHidden = true;
                }
            }
        }, fail: { (error) in
            CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""),
            msg: NSLocalizedString("ErrorGraph", comment: ""),
            h1Text: NSLocalizedString("Close", comment: ""),
            h1: {(alert: UIAlertAction!) in
                Navigation.pop();
            });
            self.activityIndicator.isHidden = true;
        })
    }
    
    func CreateObserver(){
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            self.update()
        }
    }
}
