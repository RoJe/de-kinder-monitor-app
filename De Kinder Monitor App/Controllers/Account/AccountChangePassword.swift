//
//  AccountChangePassword.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 1/5/20.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

class AccountChangePassword : UIViewController{
    // MARK: - Init
    
    /// The old password
    @IBOutlet weak var OldPasswordTextbox: UITextField!
    
    /// The new password
    @IBOutlet weak var NewPasswordTextbox: UITextField!
    
    /// The password conformation
    @IBOutlet weak var NewPasswordConf: UITextField!
    
    
    /// Spin my world.. you know the drill
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    /// Basic function
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialize()
    }
    
    /// Initialize function
    private func initialize(){
        
    }
    
    /// function to change the password in the api when the button has been pressed
    /// - Parameter sender: the button
    @IBAction func ChangePasswordClick(_ sender: Any) {
        if let err = User.CorrectPassword(password: self.OldPasswordTextbox.text ?? "", nil) {
            CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""), msg: err, h1Text: NSLocalizedString("Close", comment: ""), h1: nil)
        }
        if let err = User.CorrectPassword(password: self.NewPasswordTextbox.text ?? "", self.NewPasswordConf.text ?? "") {
            CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""), msg: err, h1Text: NSLocalizedString("Close", comment: ""), h1: nil)
        }
        if(self.NewPasswordTextbox != nil) {
            activityIndicator.isHidden = false;
            Network.UpdateUserPassword(
                password: self.NewPasswordTextbox.text!,
                urlFail: {
                    Network.netAlert()
            }, comp: { (json, error, response) in
                if response.response?.statusCode == 200 {
                    self.activityIndicator.isHidden = true;
                    CustomAlert.Show(title: NSLocalizedString("Succes", comment: ""),
                            msg: NSLocalizedString("PassChange", comment: ""),
                            h1Text: NSLocalizedString("Close", comment: ""),
                            h1: {(alert: UIAlertAction!) in
                                Navigation.pop();
                            });
                    
                    return
                }
            }, fail: { error in
                Network.netAlert()
            })
        }


    }
}
