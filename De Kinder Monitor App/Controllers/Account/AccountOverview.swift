//
//  AccountOverzicht.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 1/5/20.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

class AccountOverview : UIViewController{
    
    
    /// The big username label
    @IBOutlet weak var UserNameBigLabel: UILabel!
    
    /// Email label
    @IBOutlet weak var emailLabel: UILabel!
    
    /// First name label
    @IBOutlet weak var FirstNameLabel: UILabel!
    
    /// Last anme label
    @IBOutlet weak var LastNameLabel: UILabel!

    /// Day of birth label
    @IBOutlet weak var BirthDayLabel: UILabel!
    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
    }
    
    // MARK: - Handlers
    
    /// Initialize function
    private func initialize(){
        let user = User.GetData();
        if (user != nil ) {
            self.UserNameBigLabel.text = user?.username
            self.emailLabel.text = user?.email
            self.FirstNameLabel.text = user?.firstName
            self.LastNameLabel.text = user?.lastName
            self.BirthDayLabel.text = user?.phoneNumber
        }
    }
    
    
    /// Function for when the loggout button has bene pressed
    /// - Parameter sender: the button
    @IBAction func logoutButtonClick(_ sender: Any) {
        User.UserLogout()
    }
}
