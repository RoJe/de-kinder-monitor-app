//
//  AccountEmergencyNumber.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 1/5/20.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit


class AccountEmergencyNumber : UIViewController{
    
    /// The label with the user his emergency phone number
    @IBOutlet weak var numberLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialize()
    }
    
    private func initialize(){
        let user = User.GetData();
        if (user != nil) {
            numberLabel.text = user?.phoneNumber;
        }
    }
}
