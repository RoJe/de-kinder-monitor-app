//
//  Account.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/10/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

class Account : UIViewController{
    
    @IBOutlet weak var UserNameBigLabel: UILabel!
    
    // MARK: - Properties
    @IBOutlet weak var LogOutButton: UIButton!
    var centerController: UIViewController!
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
    }
    
    // MARK: - Handlers
    
    private func initialize(){
        self.UserNameBigLabel.text = User.GetActiveUser()?.username
    }
    @IBAction func LogOutButtonClick(_ sender: Any) {
        User.Logout()
        Navigation.ReplaceRoot(strb: "LandingScreen", view: "LandingView")
        dismiss(animated: true, completion: nil)
    }
}
