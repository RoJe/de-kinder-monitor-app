//
//  VideoFeed.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 11/12/2019.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation

import UIKit
import WebKit

/// Because we miss hardware, here have a nice webview
class VideoViewController: UIViewController, WKUIDelegate {
    
    /// The webview
    @IBOutlet var webView: WKWebView!
    
    /// Load the view
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    /// Basic function
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myURL = URL(string:"https://youtu.be/KGEekP1102g") // Youtube!
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
}
