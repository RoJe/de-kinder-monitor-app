//
//  Connection.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 06/01/2020.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit


/// Class to present the user with info about their connection
class Connection:UIViewController {
    
    /// Label for lost packets
    @IBOutlet weak var lostPackageLabel: UILabel!
    
    /// Label for time
    @IBOutlet weak var timeLabel: UILabel!
    
    /// Label for the connection status
    @IBOutlet weak var activeLabel: UILabel!
    
    /// The wifi image
    @IBOutlet weak var wifiImage: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        lostPackageLabel.text = String(Sensor.getLostPackets()); 
        let startTime = Sensor.getConnectTime();
        let timeDifference = Date().timeIntervalSince(startTime)
        let seconds = Int(timeDifference)
        let days = String((seconds / 86400))
        let hours = String((seconds % 86400) / 3600)
        let minuts = String((seconds % 3600) / 60)
        let timeText = days + "d, " + hours + "h, " + minuts + "m";
        timeLabel.text = String(timeText);
        if (Sensor.getActive()) {
            activeLabel.text = NSLocalizedString("Active", comment: "");
            wifiImage.tintColor = Colors.Green;

        } else {
            activeLabel.text = NSLocalizedString("Inactive", comment: "");
            wifiImage.tintColor = Colors.Red;
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}
