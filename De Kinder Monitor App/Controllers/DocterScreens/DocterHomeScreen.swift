//
//  DocterHomeScreen.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 06/01/2020.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

class DocterHomeScreen: UIViewController {
    
    /// Fallback patient name
    var patientName = "Onbekend"
    
    /// Fallback patient id
    var patientID = 0;
    
    /// Fallback Patient value index
    var rowIndex = 0;
    
    /// Heartbeat label
    @IBOutlet weak var HeartBeatLabel: UILabel!
    
    /// Temperature label
    @IBOutlet weak var tempLabel: UILabel!
    
    /// Frequancy label
    @IBOutlet weak var freqLabel: UILabel!
    
    /// Saturation label
    @IBOutlet weak var saturationLabel: UILabel!
    
    /// Patient name label
    @IBOutlet weak var patientNameLabel: UILabel!
    
    
    /// Heartbeat limit label
    @IBOutlet weak var heartLimitLabel: UILabel!
    
    /// Temperature limit label
    @IBOutlet weak var tempLimitLabel: UILabel!
    
    /// Frequancy limit label
    @IBOutlet weak var freqLimitLabel: UILabel!
    
    /// Saturation limit label
    @IBOutlet weak var satLimitLabel: UILabel!
    
    /// Heartbeat objject
    @IBOutlet weak var heartBeatView: WhiteContentBoxPressable!
    
    /// Tempertaure object
    @IBOutlet weak var tempView: WhiteContentBoxPressable!
    
    /// Saturation object
    @IBOutlet weak var satView: WhiteContentBoxPressable!
    
    /// Frequancy object
    @IBOutlet weak var freqView: WhiteContentBoxPressable!
    
    
    /// Hearthbeat image
    @IBOutlet weak var heartImage: UIImageView!
    
    /// temperature image
    @IBOutlet weak var tempImage: UIImageView!
    
    /// Saturation image
    @IBOutlet weak var satImage: UIImageView!
    
    /// Frequancy image
    @IBOutlet weak var freqImage: UIImageView!
    
    /// Heartbeat alert icon
    @IBOutlet weak var heartAlertIcon: UIImageView!
    
    /// Frequancy alert icon
    @IBOutlet weak var freqAlertIcon: UIImageView!
    
    /// Temperature alert icon
    @IBOutlet weak var tempAlertIcon: UIImageView!
    
    /// Saturation alert icon
    @IBOutlet weak var satAlertIcon: UIImageView!
    
    
    /// Heartbeat header label
    @IBOutlet weak var heartHeaderLabel: UILabel!
    
    /// Temperature header label
    @IBOutlet weak var tempHeaderLabel: UILabel!
    
    /// Frequancy header label
    @IBOutlet weak var freqHeaderLabel: UILabel!
    
    /// Saturatyion header label
    @IBOutlet weak var satHeaderLabel: UILabel!
    
    
    /// Frequancy time label
    @IBOutlet weak var freqTimeLabel: UILabel!
    
    /// Temperature time label
    @IBOutlet weak var tempTimeLabel: UILabel!
    
    /// Heart time label
    @IBOutlet weak var heartTimeLabel: UILabel!
    
    /// Saturation time label
    @IBOutlet weak var satTimeLabel: UILabel!
    
    
    /// Dat formatter for later
    private var dateFormatter: DateFormatter?
    
    /// Logout button pressed function
    /// - Parameter sender: the button
    @IBAction func logoutPressed(_ sender: Any) {
        User.UserLogout()
        //dismiss(animated: true, completion: nil)
    }
    
    /// Overview button pressed
    /// - Parameter sender: the button
    @IBAction func overViewPressed(_ sender: Any) {
        Navigation.pop();
        //Navigation.ReplaceRoot(strb: "Docter", view: "DocterOverviewNavController")
    }
    
    /// Heartbeat 'button' tapped
    /// - Parameter sender: the button
    @IBAction func HeartTapped(_ sender: Any) {
        TableItemTapped(type: 0)
    }
    
    /// Frequancy 'button' tapped
    /// - Parameter sender: the button
    @IBAction func BreathTapped(_ sender: Any) {
        TableItemTapped(type: 1)
    }
    
    /// Saturation 'button' tapped
    /// - Parameter sender: the button
    @IBAction func SatTapped(_ sender: Any) {
        TableItemTapped(type: 2)
    }
    
    /// Temperature 'button' tapped
    /// - Parameter sender: the button
    @IBAction func TempTapped(_ sender: Any) {
        TableItemTapped(type: 3)
    }
    
    /// Initialize function
    private func initialize() {
        patientNameLabel.text = patientName;
        self.dateFormatter = DateFormatter()
        self.dateFormatter?.dateFormat = "HH:mm";
        CreateObserver();
    }
    
    /// Function to create a timer to update the sensor values
    func CreateObserver(){
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            self.MonitorSensorData();
        }
    }
    
    /// Function to controll the apperance
    /// - Parameter warning: Is there a warning?
    func HearthControll(warning: Bool) {
        if warning {
            self.heartBeatView.backgroundColor = Colors.TransParentRed
            self.heartImage.tintColor = Colors.Red;
            self.HeartBeatLabel.textColor = Colors.Red;
            self.heartHeaderLabel.textColor = Colors.Red;
            self.heartAlertIcon.isHidden = false;
            self.heartTimeLabel.text = self.dateFormatter?.string(from: Date());
            return
        }
        
        self.heartBeatView.backgroundColor = .white;
        self.heartImage.tintColor = Colors.Green;
        self.HeartBeatLabel.textColor = Colors.Green;
        self.heartHeaderLabel.textColor = Colors.Green;
        self.heartAlertIcon.isHidden = true;
        self.heartTimeLabel.text = self.dateFormatter?.string(from: Date());
        self.heartBeatView.backgroundColor = Colors.TransParentRed
        self.heartImage.tintColor = Colors.Red;
        self.HeartBeatLabel.textColor = Colors.Red;
        self.heartHeaderLabel.textColor = Colors.Red;
        self.heartAlertIcon.isHidden = false;
    }
    
    /// Function to controll the apperance
    /// - Parameter warning: Is there a warning?
    func TempControll(warning: Bool){
        if warning {
            self.tempView.backgroundColor = Colors.TransParentRed
            self.tempImage.tintColor = Colors.Red;
            self.tempLabel.textColor = Colors.Red;
            self.tempHeaderLabel.textColor = Colors.Red;
            self.tempAlertIcon.isHidden = false;
            
            self.tempTimeLabel.text = self.dateFormatter?.string(from: Date());
            return
        }
        self.tempView.backgroundColor = .white
        self.tempImage.tintColor = Colors.Green;
        self.tempLabel.textColor = Colors.Green;
        self.tempHeaderLabel.textColor = Colors.Green;
        self.tempAlertIcon.isHidden = true;
        self.tempTimeLabel.text = self.dateFormatter?.string(from: Date());
    }
    
    /// Function to controll the apperance
    /// - Parameter warning: Is there a warning?
    func FreqControll(warning: Bool){
        if warning {
            self.freqImage.tintColor = Colors.Red;
            self.freqLabel.textColor = Colors.Red;
            self.freqHeaderLabel.textColor = Colors.Red;
            self.freqView.backgroundColor = Colors.TransParentRed;
            self.freqAlertIcon.isHidden = false;
            self.freqTimeLabel.text = self.dateFormatter?.string(from: Date());
            return
        }
        self.freqView.backgroundColor = .white;
        self.freqImage.tintColor = Colors.Green;
        self.freqLabel.textColor = Colors.Green;
        self.freqHeaderLabel.textColor = Colors.Green;
        self.freqAlertIcon.isHidden = true;
        self.freqTimeLabel.text = self.dateFormatter?.string(from: Date());
    }
    
    /// Function to controll the apperance
    /// - Parameter warning: Is there a warning?
    func SatControll(warning: Bool){
        if warning {
            self.satImage.tintColor = Colors.Red;
            self.saturationLabel.textColor = Colors.Red;
            self.satHeaderLabel.textColor = Colors.Red;
            self.satAlertIcon.isHidden = false;
            self.satView.backgroundColor = Colors.TransParentRed;

            self.satTimeLabel.text = self.dateFormatter?.string(from: Date());
            return
        }
        self.satView.backgroundColor = .white;
        self.satImage.tintColor = Colors.Green;
        self.saturationLabel.textColor = Colors.Green;
        self.satHeaderLabel.textColor = Colors.Green;
        self.satAlertIcon.isHidden = true;
        self.satTimeLabel.text = self.dateFormatter?.string(from: Date());
    }
    
    
    /// Function to get the sensor limits
    func getSensorLimits() {
        
        let tempLimits = Sensor.GetDocterTemperatureLimits(id: patientID);
        if(tempLimits != nil) {
            tempLimitLabel.text = String(tempLimits!.min) + " - " + String(tempLimits!.max) + NSLocalizedString("DegreeType", comment: "");
        } else {
            tempLimitLabel.text = "E - E";
        }
        
        let freqLimits = Sensor.GetDocterFrequencyLimits(id: patientID);
        if(freqLimits != nil) {
            freqLimitLabel.text = String(freqLimits!.min) + " - " + String(freqLimits!.max) + "x";
        } else {
            freqLimitLabel.text = "E - E";
        }
        let heartLimits = Sensor.GetDocterHeartBeatLimits(id: patientID);
        if(freqLimits != nil) {
            heartLimitLabel.text = String(heartLimits!.min) + " - " + String(heartLimits!.max);
        } else {
            heartLimitLabel.text = "E - E";
        }
        self.HearthControll(warning: !Sensor.IsWithinDocterHeartBeatBounds(id: patientID, rowIndex: rowIndex))
        self.TempControll(warning: !Sensor.IsWithinDocterTemperatureBounds(id: patientID, rowIndex: rowIndex))
        self.FreqControll(warning: !Sensor.IsWithinDocterFrequencyBounds(id: patientID, rowIndex: rowIndex))
        self.SatControll(warning: !Sensor.IsWithinDocterSaturationBounds(id: patientID, rowIndex: rowIndex))
        
        let satLimits = Sensor.GetDocterSaturationLimits(id: patientID);
        if(satLimits != nil) {
            satLimitLabel.text = String(satLimits!.min) + " - " + String(satLimits!.max);
        } else {
            satLimitLabel.text = "E - E";
        }
    }
    
    /// Base function
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize();
    }
    
    /// Function to create a socket
    private func CreateSocket(){
            Network.CreateSocketConnection(url: "http://51.136.23.80:8080", onConnect: {
            if (!Network.GetSocketConnection()!.SendText(msg: "Sensordata")){
                Network.netAlert()
            }
            self.getSensorLimits();
           },onDisconnect: { err in
                self.getSensorLimits();
            },onText: { text in
            do {
                let data = try JSONDecoder().decode(Array<SensorSocketData>.self, from: Data(text?.utf8 ?? "".utf8) )
                Sensor.SetDocterSensorData(values: data)
                self.MonitorSensorData()
            } catch{
                
            }

                
            DispatchQueue.main.asyncAfter(deadline: .now() + Sensor.GetSensorUpdateInterval()) {
                   if (!Network.GetSocketConnection()!.SendText(msg: "Sensordata")){
                    Network.netAlert()
                   }
                    self.getSensorLimits();
                }
           }, onData: { data in
                self.getSensorLimits();
           })
       }
    
    
    /// See which item has been tapped and react acordangly
    /// - Parameter type: the thing tapped
    func TableItemTapped( type: Int) {
        switch type {
        case 0:
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Docter", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DocterHeartScreen") as! DocterHeartBeatScreen;
            viewController.patientID = patientID;
            viewController.rowIndex = rowIndex;
            Navigation.PresentWithController(view: viewController, anim: true);
        case 1:
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Docter", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DocterBreathScreen") as! DocterFreqScreen;
            viewController.patientID = patientID;
            viewController.rowIndex = rowIndex;
            Navigation.PresentWithController(view: viewController, anim: true);
        case 2:
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Docter", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DocterSatScreen") as! DocterSatScreen;
            viewController.patientID = patientID;
            viewController.rowIndex = rowIndex;
            Navigation.PresentWithController(view: viewController, anim: true);
        case 3:
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Docter", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DocterTempScreen") as! DocterTempScreen;
            viewController.patientID = patientID;
            viewController.rowIndex = rowIndex;
            Navigation.PresentWithController(view: viewController, anim: true);
        default:
            CustomAlert.Show(title: NSLocalizedString("UnknownActionTitle", comment: ""), msg: NSLocalizedString("UnknownAction", comment: ""), h1Text: NSLocalizedString("Close", comment: ""), h1: nil)
        }
    }
    
    /// Function to get the data from the sensor class
    private func MonitorSensorData(){
        self.HeartBeatLabel.text    = Sensor.GetDocterHeartBeat(rowIndex: rowIndex);
        self.tempLabel.text         = Sensor.GetDocterTemperature(rowIndex: rowIndex);
        self.freqLabel.text         = Sensor.GetDocterFrequency(rowIndex: rowIndex);
        self.saturationLabel.text   = Sensor.GetDocterSaturation(rowIndex: rowIndex);
        getSensorLimits();
    }
}
