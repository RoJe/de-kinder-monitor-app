//
//  DocterOverviewTable.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 05/01/2020.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit




extension DocterLandingPage: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.patientList.count == 0) {
            self.emptyListLabel.text = NSLocalizedString("EmptyPatList", comment: "");
        }
        return self.patientList.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "docteroverviewcell", for: indexPath) as! docteroverviewcell;
        let patientData = self.patientList[indexPath.row];
        
        
        let docterTempLimits = Sensor.GetDocterTemperatureLimits(id: patientData!.patientID);
        let docterFreqLimits = Sensor.GetDocterFrequencyLimits(id: patientData!.patientID);
        let docterHeartLimits = Sensor.GetDocterHeartBeatLimits(id: patientData!.patientID);
        let docterSatLimits = Sensor.GetDocterSaturationLimits(id: patientData!.patientID);
        
        if(indexPath.row < self.sensorData.count ) {
            cell.patientHeartRate.text = self.sensorData[indexPath.row].Heart;
            cell.patientFreq.text = self.sensorData[indexPath.row].Freq
            cell.patientSat.text = self.sensorData[indexPath.row].Sat
            cell.patientTemp.text = self.sensorData[indexPath.row].Temp
        } else {
            cell.patientHeartRate.text = "E";
            cell.patientFreq.text = "E";
            cell.patientSat.text = "E";
            cell.patientTemp.text = "E";
        }
        
        var error = false;
        if(!Sensor.IsWithinDocterTemperatureBounds(id: patientData!.patientID, rowIndex: indexPath.row)) {
            cell.tempIcon.tintColor = Colors.Red;
            cell.errorIcon.isHidden = false;
            cell.patientName.textColor = Colors.Red;
            cell.errorTime.text = self.dateFormatter?.string(from: Date());
            cell.errorTime.isHidden = false
            error = true
        } else {
            cell.tempIcon.tintColor = Colors.Green;
        }
        if(!Sensor.IsWithinDocterSaturationBounds(id: patientData!.patientID, rowIndex: indexPath.row)) {
            cell.satIcon.tintColor = Colors.Red;
            cell.errorIcon.isHidden = false;
            cell.patientName.textColor = Colors.Red;
            cell.errorTime.text = self.dateFormatter?.string(from: Date());
            cell.errorTime.isHidden = false
            error = true
        } else {
            cell.satIcon.tintColor = Colors.Green;
        }
        if(!Sensor.IsWithinDocterFrequencyBounds(id: patientData!.patientID, rowIndex: indexPath.row)) {
            cell.freqIcon.tintColor = Colors.Red;
            cell.errorIcon.isHidden = false;
            cell.patientName.textColor = Colors.Red;
            cell.errorTime.text = self.dateFormatter?.string(from: Date());
            cell.errorTime.isHidden = false
            error = true
        } else {
            cell.freqIcon.tintColor = Colors.Green;
        }
        if(!Sensor.IsWithinDocterHeartBeatBounds(id: patientData!.patientID, rowIndex: indexPath.row)) {
            cell.heartIcon.tintColor = Colors.Red;
            cell.errorIcon.isHidden = false;
            cell.patientName.textColor = Colors.Red;
            cell.errorTime.text = self.dateFormatter?.string(from: Date());
            cell.errorTime.isHidden = false
            error = true
        } else {
            cell.heartIcon.tintColor = Colors.Green;
        }
        if(!error){
            cell.errorIcon.isHidden = true;
            cell.patientName.textColor = Colors.Green;
            cell.errorTime.isHidden = true
        } else {
            for item in tempShuffleOrder {
                if(item == indexPath.row) {
                    let tempData = tempShuffleOrder[indexPath.row];
                    tempShuffleOrder.remove(at: indexPath.row);
                    tempShuffleOrder.insert(tempData, at: 0);
                }
            }
        }
        
        if(listMayShuffle()) {
            if(tempShuffleOrder.count == shuffleOrder.count && tempShuffleOrder[0] != nil) {
                shuffleOrder = tempShuffleOrder;
                shufflePatient(patientList);
                shuffleData(sensorData);
            }
        }
        
        cell.patientName.text = patientData!.firstname + " " + patientData!.lastname;
        cell.tempLimitLabel.text = String(docterTempLimits!.min) + " - " + String(docterTempLimits!.max) + NSLocalizedString("DegreeType", comment: "");
        cell.freqLimitLabel.text = String(docterFreqLimits!.min) + " - " + String(docterFreqLimits!.max) + "x";
        cell.heartLimitLabel.text = String(docterHeartLimits!.min) + " - " + String(docterHeartLimits!.max);
        cell.satLimitLabel.text = String(docterSatLimits!.min) + " - " + String(docterSatLimits!.max) + " %";
        return cell
    }

    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Docter", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DocterHomeScreen") as! DocterHomeScreen;
        if(self.patientList[indexPath.row] != nil) {
            viewController.patientName = "- " + self.patientList[indexPath.row]!.firstname + " " + self.patientList[indexPath.row]!.lastname;
            viewController.patientID = self.patientList[indexPath.row]!.patientID;
            viewController.rowIndex = indexPath.row;
            Navigation.GoWithController(view: viewController, anim: true);
        }
    }
}

class DocterLandingPage: UIViewController {
    private var dateFormatter: DateFormatter?
    private var patientList = [PatientData?]();
    private var sensorData = [SensorSocketData]();
    private var shuffleTime: Double = 0;
    private var nextShuffleTime: Double = 5;
    private var shuffleOrder = [nil] as [Int?];
    private var tempShuffleOrder = [nil] as [Int?];
    private var shuffledPatientList = [PatientData?]()
    @IBOutlet weak var activitiyIndicator: UIActivityIndicatorView!
    
    @IBAction func LogOutButtonClick(_ sender: Any) {
        User.UserLogout();
    }

    @IBOutlet weak var emptyListLabel: UILabel!
    @IBOutlet var docterTableView: UITableView!

    let cellReuseIdentifier = "cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize();
    }

    func listMayShuffle() -> Bool{
        if shuffleTime < CACurrentMediaTime() {
            shuffleOrder = [];
            for i in 0...(patientList.count - 1) {
                shuffleOrder.append(i);
            }
            shuffleTime = CACurrentMediaTime() + nextShuffleTime;
            return true
        }
        return false
    }
    
    func shufflePatient(_ data: [PatientData?]) {
        if( shuffleOrder.count == data.count && shuffleOrder[0] != nil ) {
            var newData = data;
            for i in 0...(data.count - 1) {
                let tempData = newData[shuffleOrder[i]!];
                newData.remove(at: shuffleOrder[i]!);
                newData.insert(tempData, at: 0);
            }
            patientList = newData;
        } else {
            patientList = data;
            for i in 0...(data.count - 1) {
                shuffleOrder.append(i);
            }
        }
    }
    
    func shuffleData(_ data: [SensorSocketData]) {
        if(tempShuffleOrder[0] == nil) {
            tempShuffleOrder = shuffleOrder;
        }
        if( shuffleOrder.count == data.count && shuffleOrder[0] != nil ) {
        var newData = data;
        for i in 0...(data.count - 1) {
            let tempData = newData[shuffleOrder[i]!];
            newData.remove(at: shuffleOrder[i]!);
            newData.insert(tempData, at: 0)

            }
        sensorData = newData;
        } else {
            sensorData = data;
            for i in 0...(data.count - 1) {
                shuffleOrder.append(i);
            }
        }
        Sensor.SetDocterSensorData(values: sensorData);
        self.docterTableView.reloadData();
    }
    
    func initialize() {
        dateFormatter = DateFormatter()
        dateFormatter?.dateFormat = "HH:mm";
        activitiyIndicator.isHidden = false;
        emptyListLabel.text = "";
        docterTableView.backgroundView = emptyListLabel;
        
        docterTableView.delegate = self
        docterTableView.dataSource = self

        docterTableView.register(UINib(nibName: "docteroverviewcell", bundle: nil), forCellReuseIdentifier: "docteroverviewcell");
        docterTableView.rowHeight = 200;
        docterTableView.separatorInset = .init(top: CGFloat(32), left: CGFloat(0), bottom: CGFloat(32), right: CGFloat(0))
        docterTableView.separatorStyle = .none;
        
        Network.GetConnectedPatients(
            urlFail: {
                self.activitiyIndicator.isHidden = true;
        }, comp: { (json, error, response) in
            if response.response?.statusCode == 200 {
                do {
                    let patients = try JSONDecoder().decode(Array<PatientData>.self, from: Data(json!.utf8))
                    self.shufflePatient(patients);
                    self.SetSocket(patients: patients);
                    self.docterTableView.reloadData();
                    self.activitiyIndicator.isHidden = true;
                    return
                } catch {
                    self.activitiyIndicator.isHidden = true;
                }
            }
        }, fail: { error in
            self.activitiyIndicator.isHidden = true;
        })
    }
    
    private func GetSocketMessage(patients: [PatientData]) -> String{
        var msg = "SensorDataMultiple,"
        if 0 <= patients.count-2{
            for n in 0...patients.count-2 {
                msg +=  "\(patients[n].patientID),"
            }
        }
        msg += "\(patients[ patients.count-1].patientID)"
        
        return msg
    }
    
    private func SetSocket(patients: [PatientData]){
        let msg: String = GetSocketMessage(patients: patients)
        
        Network.CreateSocketConnection(url: "http://51.136.23.80:8080", onConnect: {
            
            
            if (!Network.GetSocketConnection()!.SendText(msg: msg) && User.GetLoginStatus()){
                 CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""),
                                  msg: NSLocalizedString("NoServerCon", comment: ""),
                                  h1Text: NSLocalizedString("Close", comment: ""),
                                  h1: nil)
            }
        },onDisconnect: { err in
        },onText: { text in
             do {
                let tempSensorData = try JSONDecoder().decode([SensorSocketData].self, from: Data(text?.utf8 ?? "".utf8))
                self.shuffleData(tempSensorData);

             }catch {
                
             }
         DispatchQueue.main.asyncAfter(deadline: .now() + Sensor.GetSensorUpdateInterval()) {
            if (!Network.GetSocketConnection()!.SendText(msg: msg) && User.GetLoginStatus()){
                 CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""),
                                  msg: NSLocalizedString("NoServerCon", comment: ""),
                                  h1Text: NSLocalizedString("Close", comment: ""),
                                  h1: nil)
            }
         }
        }, onData: { data in
        })
    }
    
}


