//
//  DocterSatScreen.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 26/01/2020.
//  Copyright © 2020 FixIt. All rights reserved.
//

import Foundation
import UIKit

/// The detail screen for the docter saturation
class DocterSatScreen: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    /// Allowed times to be picked
    private let graphTimeDate = [61, 180, 1440, 4320]
    
    /// The allowed date formats
    private let dateFormats = ["HH:mm:ss", "HH:mm:ss", "HH:mm", "dd HH", "HH:mm:ss"]
    
    /// The data insite the pickers
    var pickerData: [String] = [String]()
    
    /// Current picker being used
    var activePicker: Int = 0
    
    /// Current patient id
    var patientID = 0;
    
    /// Current index for the values
    var rowIndex = 0;
    
    /// Current select minutes
    private var minutes: Int = 61

    /// the picker
    @IBOutlet weak var uiPicker: UIPickerView!

    /// The chart being used to display data
    @IBOutlet weak var LineChart: LineChartTests!

    /// The sensor value
    @IBOutlet weak var sensorValue: UILabel!

    /// The current icon
    @IBOutlet weak var sensorStatusIcon: UIImageView!
    
    /// Spin me round baby right round..
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// The graph
    @IBOutlet weak var graphTime: UISegmentedControl!
    
    /// min picker button
    @IBOutlet weak var minPicker: PickerButton!
    
    /// max picker button
    @IBOutlet weak var maxPicker: PickerButton!
    
    /// alarm time picker button
    @IBOutlet weak var alarmPicker: PickerButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetPlaceHolders()
        self.update()
        self.CreateObserver()
        self.GefreshPickerData()
        
        uiPicker.dataSource = self;
        uiPicker.delegate = self;
    }
    
    /// When the time picker is pressed
    /// - Parameter sender: the button
    @IBAction func graphTimeChanged(_ sender: Any) {
        let time = self.graphTimeDate[graphTime.selectedSegmentIndex]
        self.minutes = time
        self.GefreshPickerData()
    }
    
    /// When the min picker is pressed
    /// - Parameter sender: the button
    @IBAction func minPickerPressed(_ sender: Any) {
        activePicker = 0;
        uiPicker.isHidden = false;
        pickerData = GetRangeValues(true);
        self.uiPicker.reloadAllComponents()
        
    }
    
    // When the max picker is pressed
    /// - Parameter sender: the button
    @IBAction func maxPickerPressed(_ sender: Any) {
        activePicker = 1;
        uiPicker.isHidden = false;
        pickerData = GetRangeValues(false);
        self.uiPicker.reloadAllComponents()
    }
    
    /// When the alarm picker is pressed
    /// - Parameter sender: the button
    @IBAction func alarmPickerPressed(_ sender: Any) {
        activePicker = 2;
        uiPicker.isHidden = false;
        pickerData = ["1m", "5m", "10m", "15m", "20m"];
        uiPicker.reloadAllComponents()
    }
    
    /// Get the number of components inside the picker
    /// - Parameter pickerView: the pickerview
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
     // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int)-> Int {
        return pickerData.count
    }
        
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    /// Update the sensor value and icon
     func update() {
        let sensorText = Sensor.GetDocterSaturation(rowIndex: rowIndex) + NSLocalizedString("DegreeType", comment: "");
        if(!Sensor.IsWithinDocterSaturationBounds(id: patientID, rowIndex: rowIndex)) {
            sensorStatusIcon.tintColor = Colors.Red
            sensorValue.textColor = Colors.Red;
            sensorValue.text = sensorText;
            return
        }
        sensorStatusIcon.tintColor = Colors.Green
        sensorValue.textColor = Colors.Green;
        
        sensorValue.text = sensorText;
    }
    
    /// Set the correct limir from the picker
    /// - Parameters:
    ///   - pickerView: the pickerview
    ///   - row: the selected row
    ///   - component: the component
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let textData = pickerData[row];
        switch(activePicker) {
            case 0:
                let patientId = patientID
                let minValue = Int(textData)!;
                let maxValue = (Sensor.GetDocterSaturationLimits(id: patientId)?.max)!;
                minPicker.setTitle(String(pickerData[row]), for: UIControl.State.normal)
                Sensor.SetDocterSaturationLimits(min: minValue, max: maxValue, id: patientId)
                uiPicker.isHidden = true
                break;
            case 1:
                let patientId = patientID;
                let minValue = Sensor.GetDocterSaturationLimits(id: patientId)!.min;
                let maxValue = Int(textData)!;
                maxPicker.setTitle(String(pickerData[row]), for: UIControl.State.normal)
                Sensor.SetDocterSaturationLimits(min: minValue, max: maxValue, id: patientId)
                uiPicker.isHidden = true
                break;
            default:
                break;
        }
    }
    
    /// Load the placeholder when the view is opend
    private func SetPlaceHolders(){
        let patientId = patientID;
       let limits = Sensor.GetDocterSaturationLimits(id: patientId);
        if(limits != nil) {
            minPicker.setTitle(String(limits!.min), for: UIControl.State.normal)
            maxPicker.setTitle(String(limits!.max), for: UIControl.State.normal)
        }
    }
    
    /// Get the new range values
    /// - Parameter type: which direction?
    private func GetRangeValues(_ type: Bool) -> [String]{
        if(type) {
            var values = [String]()
            let fallMin = Sensor.getSaturationFallback().min
            let limitMax = Sensor.GetDocterSaturationLimits(id: patientID)!.max;
            for i in fallMin...limitMax {
                values.append(String(i))
            }
            return values
        } else {
            var values = [String]()
            let limitMin = Sensor.GetDocterSaturationLimits(id: patientID)!.min;
            let fallMax = Sensor.getSaturationFallback().max
            
            for i in limitMin...fallMax {
                values.append(String(i))
            }
            return values;
        }
    }
    
    /// TO MANY VALUES
    override func didReceiveMemoryWarning() {
       super.didReceiveMemoryWarning()
    }
    
    /// Get the new graph data
    private func GefreshPickerData(){
        Network.GetConnectedPatients(urlFail: {
        }, comp: { (json, error, response) in
            if let json = json {
                do {
                    let result = try JSONDecoder().decode([PatientData].self, from: Data(json.utf8) )
                    
                    let currDate = Date()
                    Network.GetSensorData(
                        name: "Sat",
                        patientId: result.first?.patientID ?? 0,
                        from: currDate.adding(minutes: -self.minutes),
                        to: currDate,
                        urlFail: {
                            Network.netAlert()
                        }, comp: { (json, error, response) in
                            do {
                                if let json = json {
                                    let dateFormatterGet = DateFormatter()
                                    dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
                                    
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = self.dateFormats[self.graphTime.selectedSegmentIndex]
                                    
                                    let pickerData = try JSONDecoder().decode(GraphPointSimple.self, from: Data(json.utf8) )
                                    self.activityIndicator.isHidden = true;
                                    let dateObjects = (pickerData.time.compactMap { dateFormatterGet.date(from: $0) }).compactMap { dateFormatter.string(from: $0) }
                                    self.LineChart.setChart(dataPoints: dateObjects, values: pickerData.value);
                                }
                            }catch{
                                Network.netAlert()
                                self.activityIndicator.isHidden = true;
                            }
                            
                        }, fail:{ error in
                            Network.netAlert()
                    })
                }catch{
                    Network.netAlert()
                    self.activityIndicator.isHidden = true;
                }
            }
        }, fail: { (error) in
            self.activityIndicator.isHidden = true;
        })
    }
    
    func CreateObserver(){
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            self.update()
        }
    }
}
