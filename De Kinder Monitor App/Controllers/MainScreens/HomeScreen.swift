//
//  HomeScreen.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 09/12/2019.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit


class HomeScreen: UIViewController, UIGestureRecognizerDelegate{
    
    /// Heartbeat button thingy
    @IBOutlet weak var heartBeatView: WhiteContentBoxPressable!
    
    /// Temperature button thingy
    @IBOutlet weak var tempView: WhiteContentBoxPressable!
    
    /// Saturation button thingy
    @IBOutlet weak var satView: WhiteContentBoxPressable!
    
    /// Frequancy button thingy
    @IBOutlet weak var freqView: WhiteContentBoxPressable!
    
    /// Connection button thingy
    @IBOutlet weak var connectionView: WhiteContentBoxPressable!
    
    /// Adduser button thingy
    @IBOutlet weak var addUserView: WhiteContentBoxPressable!
    
    /// Heartbeat image
    @IBOutlet weak var heartImage: UIImageView!
    
    /// Temperature image
    @IBOutlet weak var tempImage: UIImageView!
    
    /// Saturation image
    @IBOutlet weak var satImage: UIImageView!
    
    /// Frequancy image
    @IBOutlet weak var freqImage: UIImageView!
    
    /// connection image
    @IBOutlet weak var connectionImage: UIImageView!
    
    /// Webcam image
    @IBOutlet weak var webcamImage: UIImageView!
    
    /// Heart alert icon
    @IBOutlet weak var heartAlertIcon: UIImageView!
    
    /// Frequancy alert icon
    @IBOutlet weak var freqAlertIcon: UIImageView!
    
    /// Temperature  alert icon
    @IBOutlet weak var tempAlertIcon: UIImageView!
    
    /// Saturation alert icon
    @IBOutlet weak var satAlertIcon: UIImageView!
    
    /// Webcam alert icon
    @IBOutlet weak var webcamAlertIcon: UIImageView!
    
    /// Connection alert icon
    @IBOutlet weak var connectionAlertIcon: UIImageView!
    
    /// Heart header label
    @IBOutlet weak var heartHeaderLabel: UILabel!
    
    /// Temperature header label
    @IBOutlet weak var tempHeaderLabel: UILabel!
    
    /// Frequancy header label
    @IBOutlet weak var freqHeaderLabel: UILabel!
    
    /// Saturation header label
    @IBOutlet weak var satHeaderLabel: UILabel!
    
    /// Connection header label
    @IBOutlet weak var connectionHeaderLabel: UILabel!
    
    /// Webcam header label
    @IBOutlet weak var webcamHeaderLabel: UILabel!
    
    /// Frequancy time label
    @IBOutlet weak var freqTimeLabel: UILabel!
    
    /// Temperature time label
    @IBOutlet weak var tempTimeLabel: UILabel!
    
    /// Heart time label
    @IBOutlet weak var heartTimeLabel: UILabel!
    
    /// Saturation time label
    @IBOutlet weak var satTimeLabel: UILabel!
    
    /// Sensor heart label
    @IBOutlet weak var heartLabel: UILabel!
    
    /// Sensor Temperature label
    @IBOutlet weak var tempLabel: UILabel!
    
    /// Sensor Frequancy label
    @IBOutlet weak var freqLabel: UILabel!
    
    /// Sensor Saturation label
    @IBOutlet weak var saturationLabel: UILabel!
    
    /// Temperature  limit label
    @IBOutlet weak var tempLimitLabel: UILabel!
    
    /// Heartbeat limit label
    @IBOutlet weak var heartLimitLabel: UILabel!
    
    /// Frequancy limit label
    @IBOutlet weak var freqLimitLabel: UILabel!
    
    /// Saturation limit label
    @IBOutlet weak var saturationLimitLabel: UILabel!
    
    /// Date formatter used later
    private var dateFormatter: DateFormatter?
    
    /// Dateformatter for the socket
    private var dateFormatterSocket: DateFormatter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let scrollView = UIScrollView(frame: self.view.frame)
        scrollView.delegate = self as? UIScrollViewDelegate
        initialize()

    }
    
    /// Initialize function
    private func initialize(){
        if (User.GetPermissionData().write ?? false) {
            addUserView.isHidden = false;
        } else {
            addUserView.isHidden = true;
        }
        self.dateFormatter = DateFormatter()
        self.dateFormatter?.dateFormat = "HH:mm";
        
        webcamHeaderLabel.textColor = Colors.Green;
        webcamImage.tintColor = Colors.Green;
        webcamAlertIcon.isHidden = true;
        self.setLimits();
        CreateSocket()
    }
    
    /// Heartbeat tapped
    /// - Parameter sender: the button thingy
    @IBAction func HeartTapped(_ sender: Any) {
        Navigation.Present(strb: "Details", view: "HeartScreen", anim: true)
    }
    
    /// Frequancy  tapped
    /// - Parameter sender: the button thingy
    @IBAction func BreathTapped(_ sender: Any) {
        Navigation.Present(strb: "Details", view: "BreathScreen", anim: true)
    }
    
    /// Saturation tapped
    /// - Parameter sender: the button thingy
    @IBAction func SatTapped(_ sender: Any) {
        Navigation.Present(strb: "Details", view: "SatScreen", anim: true)
    }
    
    /// Temperature tapped
    /// - Parameter sender: the button thingy
    @IBAction func TempTapped(_ sender: Any) {
        Navigation.Present(strb: "Details", view: "TempScreen", anim: true)
    }
    
    /// Wifi tapped
    /// - Parameter sender: the button thingy
    @IBAction func wifiTapped(_ sender: Any) {
        Navigation.Present(strb: "Connection", view: "WifiScreen", anim: true)
    }
    /// Webcam tapped
    /// - Parameter sender: the button thingy
    @IBAction func camTapped(_ sender: Any) {
        Navigation.Present(strb: "Details", view: "VideoScreen", anim: true)
    }
    
    /// Loggout tapped
    /// - Parameter sender: the button thingy
    @IBAction func LogOutButtonClick(_ sender: Any) {
        User.UserLogout()
    }

    /// Add user  tapped
    /// - Parameter sender: the button thingy
    @IBAction func AddUserTapped(_ sender: Any) {
        Navigation.Present(strb: "AddUser", view: "AddUserNavigationView", anim: true)
    }
    
    /// Account tapped
    /// - Parameter sender: the button thingy
    @IBAction func AccountTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AccountNavigationView")
        present(vc, animated: true, completion: nil)
    }
    
    /// Create the socket connection
    private func CreateSocket(){
           Network.CreateSocketConnection(url: "http://51.136.23.80:8080", onConnect: {
               if (!Network.GetSocketConnection()!.SendText(msg: "Sensordata,1") && User.GetLoginStatus()){
                    self.WifiController(warning: true)
                    Network.netAlert()
               }
           },onDisconnect: { err in
            self.WifiController(warning: true)
           },onText: { text in
                do {
                    let data = try JSONDecoder().decode(SensorSocketData.self, from: Data(text?.utf8 ?? "".utf8) )
                    Sensor.SetSensorData(values: data)
                    self.MonitorSensorData()
                }catch {
                }
                
                

            DispatchQueue.main.asyncAfter(deadline: .now() + Sensor.GetSensorUpdateInterval()) {
               if (!Network.GetSocketConnection()!.SendText(msg: "Sensordata,1") && User.GetLoginStatus()){
                    self.WifiController(warning: true)
                    Network.netAlert()
               }
            }
           }, onData: { data in
           })
    }
    
    /// Monitor the sensor values and update them
    private func MonitorSensorData(){
        self.setLimits()
        self.WifiController(warning: false)
        self.HearthControll(warning: !Sensor.IsWithinHeartBeatBounds())
        self.TempControll(warning: !Sensor.IsWithinTemperatureBounds())
        self.FreqControll(warning: !Sensor.IsWithinFrequancyBounds())
        self.SatControll(warning: !Sensor.IsWithinSaturationBounds())
        
        
        self.heartLabel.text        = Sensor.GetHeartBeat()
        self.tempLabel.text         = Sensor.GetTemperature()
        self.freqLabel.text         = Sensor.GetFrequancy()
        self.saturationLabel.text   = Sensor.GetSaturation()

    }
    
    
    /// Controller function to change stuff depending on the values from the sensor
    /// - Parameter warning: is there a warning?
    private func WifiController(warning: Bool){
        if (warning) {
            self.connectionImage.tintColor = Colors.Red;
            self.connectionHeaderLabel.textColor = Colors.Red;
            self.connectionAlertIcon.isHidden = false;
            self.connectionView.backgroundColor = Colors.TransParentRed;
            self.HearthControll(warning: true)
            self.TempControll(warning: true)
            self.FreqControll(warning: true)
            self.SatControll(warning: true)
            return
        }
        self.connectionImage.tintColor = Colors.Green;
        self.connectionAlertIcon.isHidden = true;
        self.connectionView.backgroundColor = .white;
        self.connectionHeaderLabel.textColor = Colors.Green
    }
    
    /// Controller function to change stuff depending on the values from the sensor
    /// - Parameter warning: is there a warning?
    func setLimits(){
        self.heartLimitLabel.text = "\(Sensor.GetHeartBeatLimits().min) - \(Sensor.GetHeartBeatLimits().max)"
        self.tempLimitLabel.text = "\(Sensor.GetTemperatureLimits().min) - \(Sensor.GetTemperatureLimits().max)"
        self.freqLimitLabel.text = "\(Sensor.GetFrequancyLimits().min) - \(Sensor.GetFrequancyLimits().max)"
        self.saturationLimitLabel.text = "\(Sensor.GetSaturationLimits().min) - \(Sensor.GetSaturationLimits().max)"
    }
    
    /// Controller function to change stuff depending on the values from the sensor
    /// - Parameter warning: is there a warning?
    private func HearthControll(warning: Bool){
        let warningMsg =  NSLocalizedString("HeartWarn", comment: "") + Sensor.GetHeartBeat() + NSLocalizedString("Warn2", comment: "") + String(Sensor.GetHeartBeatLimits().min) + " , " + String(Sensor.GetHeartBeatLimits().max) + NSLocalizedString("Warn3", comment: "");
        let viewController = "HeartScreen";
        if warning {
            self.heartBeatView.backgroundColor = Colors.TransParentRed
            self.heartImage.tintColor = Colors.Red;
            self.heartLabel.textColor = Colors.Red;
            self.heartHeaderLabel.textColor = Colors.Red;
            self.heartAlertIcon.isHidden = false;
            if (Sensor.HeartCanAlert()){
                Sensor.SensorAlert(warningMsg: warningMsg, viewController: viewController);
            }
            self.heartTimeLabel.text = self.dateFormatter?.string(from: Date());
            return
        }
        
        self.heartBeatView.backgroundColor = .white;
        self.heartImage.tintColor = Colors.Green;
        self.heartLabel.textColor = Colors.Green;
        self.heartHeaderLabel.textColor = Colors.Green;
        self.heartAlertIcon.isHidden = true;
        self.heartTimeLabel.text = self.dateFormatter?.string(from: Date());
    }
    
    /// Controller function to change stuff depending on the values from the sensor
    /// - Parameter warning: is there a warning?
    private func TempControll(warning: Bool){
        let warningMsg =  NSLocalizedString("TempWarn", comment: "") + Sensor.GetTemperature() + NSLocalizedString("Warn2", comment: "") + String(Sensor.GetTemperatureLimits().min) + " , " + String(Sensor.GetTemperatureLimits().max) + NSLocalizedString("Warn3", comment: "");
        let viewController = "TempScreen";
        if warning {
            self.tempView.backgroundColor = Colors.TransParentRed
            self.tempImage.tintColor = Colors.Red;
            self.tempLabel.textColor = Colors.Red;
            self.tempHeaderLabel.textColor = Colors.Red;
            self.tempAlertIcon.isHidden = false;
            if (Sensor.TempCanAlert()){
                Sensor.SensorAlert(warningMsg: warningMsg, viewController: viewController);
            }
            
            self.tempTimeLabel.text = self.dateFormatter?.string(from: Date());
            return
        }
        self.tempView.backgroundColor = .white
        self.tempImage.tintColor = Colors.Green;
        self.tempLabel.textColor = Colors.Green;
        self.tempHeaderLabel.textColor = Colors.Green;
        self.tempAlertIcon.isHidden = true;
        self.tempTimeLabel.text = self.dateFormatter?.string(from: Date());
        
    }
    
    /// Controller function to change stuff depending on the values from the sensor
    /// - Parameter warning: is there a warning?
    private func FreqControll(warning: Bool){
        let warningMsg =  NSLocalizedString("FreqWarn", comment: "") + Sensor.GetFrequancy() + NSLocalizedString("Warn2", comment: "") + String(Sensor.GetFrequancyLimits().min) + " , " + String(Sensor.GetFrequancyLimits().max) + NSLocalizedString("Warn3", comment: "");
        let viewController = "BreathScreen";
        if warning {
            self.freqImage.tintColor = Colors.Red;
            self.freqLabel.textColor = Colors.Red;
            self.freqHeaderLabel.textColor = Colors.Red;
            self.freqView.backgroundColor = Colors.TransParentRed;
            self.freqAlertIcon.isHidden = false;
            if (Sensor.FreqCanAlert()){
                Sensor.SensorAlert(warningMsg: warningMsg, viewController: viewController);
            }
            
            self.freqTimeLabel.text = self.dateFormatter?.string(from: Date());
            return
        }
        self.freqView.backgroundColor = .white;
        self.freqImage.tintColor = Colors.Green;
        self.freqLabel.textColor = Colors.Green;
        self.freqHeaderLabel.textColor = Colors.Green;
        self.freqAlertIcon.isHidden = true;
        self.freqTimeLabel.text = self.dateFormatter?.string(from: Date());
        
    }
    
    /// Controller function to change stuff depending on the values from the sensor
    /// - Parameter warning: is there a warning?
    private func SatControll(warning: Bool){
        let warningMsg =  NSLocalizedString("SatWarn", comment: "") + Sensor.GetSaturation() + NSLocalizedString("Warn2", comment: "") + String(Sensor.GetSaturationLimits().min) + " , " + String(Sensor.GetSaturationLimits().max) + NSLocalizedString("Warn3", comment: "");
        let viewController = "SatScreen";
        if warning {
            self.satImage.tintColor = Colors.Red;
            self.saturationLabel.textColor = Colors.Red;
            self.satHeaderLabel.textColor = Colors.Red;
            self.satAlertIcon.isHidden = false;
            self.satView.backgroundColor = Colors.TransParentRed;
            if (Sensor.SatCanAlert()){
                Sensor.SensorAlert(warningMsg: warningMsg, viewController: viewController);
            }
            
            self.satTimeLabel.text = self.dateFormatter?.string(from: Date());
            return
        }
        self.satView.backgroundColor = .white;
        self.satImage.tintColor = Colors.Green;
        self.saturationLabel.textColor = Colors.Green;
        self.satHeaderLabel.textColor = Colors.Green;
        self.satAlertIcon.isHidden = true;
        self.satTimeLabel.text = self.dateFormatter?.string(from: Date());
        
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
