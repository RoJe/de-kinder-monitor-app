//
//  Account.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/10/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

class Account : UIViewController{
    
    @IBOutlet weak var UserNameBigLabel: UILabel!
    
    // MARK: - Properties
    @IBOutlet weak var LogOutButton: UIButton!
    var centerController: UIViewController!
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
    }
    
    // MARK: - Handlers
    
    /// Function to initialize stuff
    private func initialize(){
        self.UserNameBigLabel.text = User.GetData()?.username
    }
    
    /// Function for when the loggout button has been pressed
    /// - Parameter sender: the button
    @IBAction func LogOutButtonClick(_ sender: Any) {
        User.UserLogout()
    }
}
