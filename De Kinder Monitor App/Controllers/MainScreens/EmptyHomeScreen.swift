//
//  EmptyHomeScreen.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 23/01/2020.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

class EmptyHomeScreen: UIViewController, UIGestureRecognizerDelegate{

    @IBOutlet var accountTapRec: [UITapGestureRecognizer]!
    
    @IBAction func AccountTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EmptyAccountNavigationView")
        present(vc, animated: true, completion: nil)
    }
    @IBAction func logoutTapped(_ sender: Any) {
        User.UserLogout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
    }
    
}
