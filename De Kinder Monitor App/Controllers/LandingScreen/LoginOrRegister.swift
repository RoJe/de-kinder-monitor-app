//
//  LoginOrRegister.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/8/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import UIKit

class LoginOrRegister : UIViewController{
    // MARK: - Properties
    var navController : UINavigationController?
    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
        
        

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)
    }
    
    
    /// REDIRECT
    func setNewPage() {
        if ( User.GetPermissionData().patientID == nil && User.GetData()?.type != "docter" ) {
            Navigation.ReplaceRoot(strb: "MainScreens", view: "EmptyMainScreensNavController");
            CustomAlert.Show(
                title: NSLocalizedString("Warning", comment: ""),
                msg: NSLocalizedString("NoChildCon", comment: ""),
                h1Text: NSLocalizedString("Close", comment: ""),
                h1: nil
            )
        } else if (User.GetPermissionData().patientID != nil  && User.GetData()?.type != "docter" ) {
            Navigation.ReplaceRoot(strb: "MainScreens", view: "MainScreensNavController")
        } else {
            Navigation.ReplaceRoot(strb: "Docter", view: "DocterOverviewNavController");
        }
    }
    
    
    // MARK: - Handlers
    
    /// Initialize function. We do check here if you're logged in tho
    private func initialize(){
        if User.GetLoginStatus() {
            Network.IsUserValid(
                urlFail: {
                    User.DirectLogout();
                },
                comp: {(json, error, response) in
                    if(response.response?.statusCode == 200){
                        let networkCallQueueGroup = DispatchGroup()
                        networkCallQueueGroup.enter()
                        User.LoginSetUserPermissions(networkCallQueueGroup);
                        networkCallQueueGroup.enter()
                        User.LoginSetUserData(networkCallQueueGroup);
                        networkCallQueueGroup.notify(queue:.main) {
                            self.setNewPage();
                        }
                    }else{
                        User.DirectLogout();
                    }
                },
                fail: { (error) in
                    User.DirectLogout();
                }
            )
        } else {
            Navigation.ReplaceRoot(strb: "LandingScreen", view: "LandingViewNavController");

        }
    }
    @objc func dismissKeyboard() {
            //Causes the view (or one of its embedded text fields) to resign the first responder status.
            view.endEditing(true)
        }
}
