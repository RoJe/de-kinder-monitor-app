//
//  Login.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/8/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation

import UIKit

class Login : UIViewController {
    
    /// The button on the login view
    @IBOutlet weak var loginButton: UIButton!
    
    /// The button to go back
    @IBOutlet weak var returnButton: UIButton!
    
    /// The username
    @IBOutlet weak var username: UITextField!
    
    /// The password
    @IBOutlet weak var password: UITextField!
    
    /// The warning label
    @IBOutlet weak var warningLabel: UILabel!
    var navController : UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize();
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)
    }

    
    // MARK: - Handlers
    private func initialize(){
        
        self.navController = UIApplication.shared.windows.first!.rootViewController as? UINavigationController
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// Go to new view
    func setNewPage() {
        activityIndicator.isHidden = false;
        if ( User.GetPermissionData().patientID == nil && User.GetData()?.type != "docter" ) {
            Navigation.ReplaceRoot(strb: "MainScreens", view: "EmptyMainScreensNavController");
            CustomAlert.Show(
                title: NSLocalizedString("Warning", comment: ""),
                msg: NSLocalizedString("NoChildCon", comment: ""),
                h1Text: NSLocalizedString("Close", comment: ""),
                h1: nil
            )
        } else if (User.GetPermissionData().patientID != nil  && User.GetData()?.type != "docter" ) {
            Navigation.ReplaceRoot(strb: "MainScreens", view: "MainScreensNavController")
        } else {
            Navigation.ReplaceRoot(strb: "Docter", view: "DocterOverviewNavController");
        }
    }
    
    /// Login button pressed. Try to login the user
    /// - Parameter sender: the button
    @IBAction func LoginPressed(_ sender: Any) {
        activityIndicator.isHidden = false;
        if let error = User.CorrectUsername(username: username.text!) {
            activityIndicator.isHidden = true;
            warningLabel.text = error
            return
        }
        
        if let error = User.CorrectPassword(password: password.text!, nil) {
            activityIndicator.isHidden = true;
            warningLabel.text = error
            return
        }
        
        Network.loginUser(username: username.text!,
                          password: password.text!,
                          urlFail: {},
                          comp: {(json, error, response) in
                            if let json = json {
                                do {
                                    let jsonObject = try JSONDecoder().decode(userToken.self, from: Data(json.utf8) )
                                    User.Login(str: jsonObject.token)
                                    Sensor.setConnectTime(time: Date.init());
                                    let networkCallQueueGroup = DispatchGroup()
                                    networkCallQueueGroup.enter();
                                    User.LoginSetUserPermissions(networkCallQueueGroup);
                                    networkCallQueueGroup.enter();
                                    User.LoginSetUserData(networkCallQueueGroup);
                                    networkCallQueueGroup.notify(queue: .main) {
                                        self.setNewPage();
                                    }
                                }catch{
                                    CustomAlert.Show(
                                        title: NSLocalizedString("Warning", comment: ""),
                                        msg: NSLocalizedString("ErrorResp", comment: ""),
                                        h1Text: NSLocalizedString("Close", comment: ""),
                                        h1: nil
                                    )
                                }
                            }
        }, fail: { error in
            self.activityIndicator.isHidden = true;
            CustomAlert.Show(
                title: NSLocalizedString("Warning", comment: ""),
                msg: NSLocalizedString("ErrorUserPass", comment: ""),
                h1Text: NSLocalizedString("Close", comment: ""),
                h1: nil
            )
        })
    }
    
    @IBAction func ReturnButtonPressed(_ sender: Any) {
        self.navController!.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
