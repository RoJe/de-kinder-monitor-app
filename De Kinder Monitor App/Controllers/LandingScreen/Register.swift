//
//  Register.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/8/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import UIKit

class Register : UIViewController{
    
    
    /// The username label
    @IBOutlet weak var username: UITextField!
    
    /// The password label
    @IBOutlet weak var password: UITextField!
    
    /// The password conformation label
    @IBOutlet weak var passwordConf: UITextField!
    
    /// the email label
    @IBOutlet weak var email: UITextField!
    
    /// The firstname label
    @IBOutlet weak var firstName: UITextField!
    
    /// The last name label
    @IBOutlet weak var lastName: UITextField!
    
    /// The day of birt label
    @IBOutlet weak var birthday: UITextField!
    
    /// The date picker
    private var datePicker: UIDatePicker?
    
    /// SPIN MY AS*...
    @IBOutlet weak var activIndicator: UIActivityIndicatorView!
    
    /// Childcode label
    @IBOutlet weak var childCode: UITextField!
    
    /// Childcode warning label
    @IBOutlet weak var ChildCodeWarningLabel: UILabel!
    
    
    /// First next button
    @IBOutlet weak var firstNextButton: UIButton!
    
    /// first back button
    @IBOutlet weak var firstBackButton: UIButton!
    
    /// Second next button
    @IBOutlet weak var secondNextButton: UIButton!
    
    /// Second back button
    @IBOutlet weak var secondBackButton: UIButton!
    
    /// Slider ffor terms and conditions
    @IBOutlet weak var termsSlider: UISwitch!
    
    /// Slider for main caretaker
    @IBOutlet weak var caretSlider: UISwitch!
    
    /// Third next button
    @IBOutlet weak var thirdNextButton: MacRounded!
    
    /// Third back button
    @IBOutlet weak var thirdBackButton: UIButton!
    
    /// Warning label for first view
    @IBOutlet weak var warningLabelRegister1: UILabel!
    
    /// Warning label for second view
    @IBOutlet weak var warningLabelRegister2: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)
    }
    
    // MARK: - Handlers
    private func initialize(){
    }
    
    
    /// Function for first next button. Check username..password and stuff
    /// - Parameter sender: the button
    @IBAction func FirstNextButtonClick(_ sender: Any) {
        if let error = User.CorrectUsername(username: username.text!) {
            warningLabelRegister1.text = error
            return
        }
        
        if let error = User.CorrectPassword(password: password.text!, passwordConf.text!) {
            warningLabelRegister1.text = error
            return
        }
        
        if (password.text != passwordConf.text){
            warningLabelRegister1.text = NSLocalizedString("ErrorMatchPass", comment: "")
            return;
        }
        
        activIndicator.isHidden = false;
        
        //TODO: ROBBERT PLS CREATE CHECKUSERNAME FUNCTION IN DATABASE.
        

        Network.checkUserName(
            username: username.text!,
            urlFail: {
                self.activIndicator.isHidden = false;
        }, comp: { (json, error, response) in
            self.activIndicator.isHidden = true;
            if response.response?.statusCode == 200 {
                self.activIndicator.isHidden = true;
                self.warningLabelRegister1.text = "Username already taken.";
            } else if (response.response?.statusCode == 401){
                self.activIndicator.isHidden = true;
                let register = self.storyboard?.instantiateViewController(withIdentifier: "Register2") as! Register
                register.username = self.username;
                register.password = self.password;
                register.passwordConf = self.password;
                self.warningLabelRegister1.text = "";
                Navigation.GoWithController(view: register, anim: true);
            } else {
                Network.netAlert();
            }
        }, fail: { (error) in
            self.activIndicator.isHidden = true;
            Network.netAlert();
        })
        return
        


    }
    
    /// First back button pressed
    /// - Parameter sender: the button
    @IBAction func FirstBackButtonClick(_ sender: Any) {
        Navigation.pop()
    }
    
    
    
    /// Second next button. check firstname... day of birth and stuf
    /// - Parameter sender: the button
    @IBAction func SecondNextButton(_ sender: Any) {

        let register = self.storyboard?.instantiateViewController(withIdentifier: "Register3") as! Register
        if let error = User.CorrectEmail(email: email.text!) {
            warningLabelRegister2.text = error
            return
        }
        
        if let error = User.CorrectFirstName(firstName: firstName.text!){
            warningLabelRegister2.text = error
            return
        }
        
        if let error = User.CorrectLastName(lastName: lastName.text!) {
            warningLabelRegister2.text = error
            return
        }
        if let error = User.CorrectBirthDay(birthday: birthday.text!) {
            warningLabelRegister2.text = error
            return
        }
        if(!termsSlider.isOn){
            warningLabelRegister2.text = NSLocalizedString("ErrorAccTerms", comment: "")
            return
        }
        if(!caretSlider.isOn){
            activIndicator.isHidden = false;
            Network.Register(
                user: UserData(
                    userID: 0,
                    username: username.text!,
                    password: password.text!,
                    firstName: firstName.text!,
                    lastName: lastName.text!,
                    phoneNumber: "06123123765",
                    email: email.text!,
                    type: "user"
                ),
                urlFail: {
                    self.activIndicator.isHidden = true;
                    Network.netAlert()
            }, comp: { (json, error, response) in
                self.activIndicator.isHidden = true;
                if response.response?.statusCode == 200 {
                    Navigation.ReplaceRoot(strb: "LandingScreen", view: "RegisterNoMainCaretaker")
                    return
                } else {
                    Network.netAlert()
                }
                
            }, fail: { error in
                self.activIndicator.isHidden = true;
                Network.netAlert()
            })
            return
        }
        register.username = username
        register.password = password
        register.passwordConf = password
        register.email      = email
        register.firstName  = firstName
        register.lastName   = lastName
        register.birthday   = birthday
        warningLabelRegister2.text = ""
        Navigation.GoWithController(view: register, anim: true)
    }
    
    /// Second back button
    /// - Parameter sender: the button
    @IBAction func SecondBackButton(_ sender: Any) {
        Navigation.pop()
    }
    
    
    /// Third back button pressed
    /// - Parameter sender: the button
    @IBAction func ThirdBackButtonClick(_ sender: Any) {
        Navigation.pop()
    }
    
    /// Make the datepicker visible
    /// - Parameter sender: the label thingy
    @IBAction func BirthdayClick(_ sender: Any) {
        if (self.datePicker != nil ){
            return
        }
        self.datePicker = UIDatePicker()
        self.datePicker!.datePickerMode = .date
        let startDate = Date();
        let calendar = Calendar.current
        let minDate = calendar.date(byAdding: .year, value: -16, to: startDate)
        self.datePicker!.maximumDate = minDate;

        self.datePicker!.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        self.birthday.inputView = self.datePicker
        
    }
    
    /// Function for when the datepicker has changed date
    /// - Parameter sender: the datepicker
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let components = sender.calendar.dateComponents([.day, .month, .year], from: sender.date)
        self.birthday.text = String(format: "%02d/%02d/%04d", components.day!, components.month!, components.year!)
    }
    
    /// Third next button pressed. Check the child code and stuff
    /// - Parameter sender: the button
    @IBAction func ThirdNextButtonClick(_ sender: Any) {
        if(!childCode.text!.CorrectChildCode){
            ChildCodeWarningLabel.text = NSLocalizedString("ErrorWrongChildCode", comment: "")
            return
        }
        activIndicator.isHidden = false;
        
        Network.Register(
            user: UserData(
                userID: 0,
                username: username.text!,
                password: password.text!,
                firstName: firstName.text!,
                lastName: lastName.text!,
                phoneNumber: "06123123765",
                email: email.text!,
                type: "user"
            ),
            urlFail: {
            Network.netAlert()
        }, comp: { (json, error, response) in
            self.activIndicator.isHidden = true;
            do {
                if response.response?.statusCode == 200 {
                    let token = try JSONDecoder().decode(userToken.self, from: Data(json!.utf8) )
                    User.Login(str: token.token);
                    User.LoginSetUserPermissions(nil);
                    User.LoginSetUserData(nil);
                    self.RegisterUserWithChild(pageType: "Register");
                    
                    
                    return
                }
            } catch {
                self.childCodeNetworkError(pageType: "Register");
            }

            CustomAlert.Show(
                title: NSLocalizedString("Warning", comment: ""),
                msg: NSLocalizedString("ErrorNoReg", comment: ""),
                h1Text: NSLocalizedString("Close", comment: ""),
                h1: nil
            )
        }, fail: { error in
            self.activIndicator.isHidden = true;
            CustomAlert.Show(
                title: NSLocalizedString("Warning", comment: ""),
                msg: NSLocalizedString("ErrorNoRegUserName", comment: ""),
                h1Text: NSLocalizedString("Close", comment: ""),
                h1: nil
            )
        })
    }
    
    /// Damn keyboard
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /// Bind the new admin caretaker
    /// - Parameter sender: the button
    @IBAction func childCodeOnly(_ sender: Any) {
        RegisterUserWithChild(pageType: "CodeOnly");
    }
    
    /// Network call to bind the user to a child
    /// - Parameter pageType: new view
    func RegisterUserWithChild(pageType: String) {
        Network.RegisterUserWithChild(PreSharedKey: childCode.text, urlFail: {
            self.childCodeUserError(pageType: pageType);
            }, comp: { (json, error, response) in
                if(pageType == "Register") {
                    self.activIndicator.isHidden = true;
                    Navigation.ReplaceRoot(strb: "LandingScreen", view: "RegisterMainCaretaker")
                } else {
                    self.activIndicator.isHidden = true;
                    CustomAlert.Show(
                        title: NSLocalizedString("Succes", comment: ""),
                        msg: NSLocalizedString("LinkSucces", comment: ""),
                        h1Text: NSLocalizedString("Relog", comment: ""),
                        h1: {(alert: UIAlertAction!) in
                            User.DirectLogout();
                        }
                    )
                }
            }, fail: { error in
                self.childCodeNetworkError(pageType: pageType);
            })
    }
    
    
    /// Wrong child code
    /// - Parameter pageType: redirect
    private func childCodeUserError(pageType: String) {
        activIndicator.isHidden = true;
        if (pageType == "Register") {
            CustomAlert.Show(
                title: NSLocalizedString("Warning", comment: ""),
                msg: NSLocalizedString("ErrorPosWrongChildCode", comment: ""),
                h1Text: NSLocalizedString("Close", comment: ""),
                h1: {(alert: UIAlertAction!) in
                    Navigation.ReplaceRoot(strb: "LandingScreen", view: "RegisterNoMainCaretaker");
                }
            )
        } else {
            self.ChildCodeWarningLabel.text = NSLocalizedString("ErrorPosWrongChildCode", comment: "")
            
        }
    }
    
    /// Api did an uhoh
    /// - Parameter pageType: the view
    private func childCodeNetworkError(pageType: String) {
        activIndicator.isHidden = true;
        if (pageType == "Register") {
            CustomAlert.Show(
                title: NSLocalizedString("Warning", comment: ""),
                msg: NSLocalizedString("ErrorResWrongChildCode", comment: ""),
                h1Text: "Verder",
                h1: {(alert: UIAlertAction!) in
                    Navigation.ReplaceRoot(strb: "LandingScreen", view: "RegisterNoMainCaretaker");
                }
            )
        } else {
            self.ChildCodeWarningLabel.text = NSLocalizedString("ErrorResp", comment: "")
        }
    }
}

