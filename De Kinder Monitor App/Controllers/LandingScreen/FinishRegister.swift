//
//  FinishRegister.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/10/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

class FinishRegister : UIViewController {
    
    
    /// Redirect!
    /// - Parameter sender: the button
    @IBAction func RegisterNoCaretakerClick(_ sender: Any) {
        Navigation.ReplaceRoot(strb: "MainScreens", view: "EmptyMainScreensNavController");
    
    }
    
    /// REDIRECT!
    /// - Parameter sender: the button
    @IBAction func RegisterCaretakerClick(_ sender: Any) {
        Navigation.ReplaceRoot(strb: "MainScreens", view: "MainScreensNavController")
    }
}
