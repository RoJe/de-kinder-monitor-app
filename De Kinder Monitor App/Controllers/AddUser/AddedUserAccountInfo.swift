//
//  AddedUserAccountInfo.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 06/01/2020.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

class AddedUserAccountInfo: UIViewController {
    
    /// The label for the user his email
    @IBOutlet weak var emailLabel: UILabel!
    
    /// The label for the user his username
    @IBOutlet weak var firstnameLabel: UILabel!
    
    /// The label for the user his last name
    @IBOutlet weak var lastnameLabel: UILabel!
    
    /// The label for the user his dya of birth
    @IBOutlet weak var birthdateLabel: UILabel!
    
    /// The label for the user his bif title name
    @IBOutlet weak var UserNameBigLabel: UILabel!
    
    
    /// The user being displayed
    var user: UserDataConnected?
    
    /// The patient of the user
    var userPatient: Int?
    convenience init() {
        self.init();
        let unknown = NSLocalizedString("Unknown", comment: ""); // Fallback
        emailLabel.text = unknown;
        firstnameLabel.text = unknown;
        lastnameLabel.text = unknown;
        birthdateLabel.text = unknown;
        UserNameBigLabel.text = unknown;
        userPatient = 0
    }
    
    func initialize() {
        if (user != nil ){
            emailLabel.text = user?.Email;
            firstnameLabel.text = user?.FirstName;
            lastnameLabel.text = user?.LastName;
            birthdateLabel.text = user?.PhoneNumber;
            UserNameBigLabel.text = user?.Username;
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        initialize();
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    /// Remove the connected user if the delete button is pressed
    /// - Parameter sender: the button clicked
    @IBAction func DeleteClick(_ sender: Any) {
        Network.RemovePermission(
            patientID: userPatient ?? 0,
            userId: user?.UserID ?? 0,
            urlFail: {
            Network.netAlert()
        }, comp: { (json, error, response) in
            let notificationNme = NSNotification.Name("NotificationIdf")
            NotificationCenter.default.post(name: notificationNme, object: nil)
            Navigation.pop()
        }, fail: {error in
            Network.netAlert()
        })
    }
}
