//
//  AddAccount.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 06/01/2020.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

/// Normal extension for a tableview.
extension AddUser: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ( tableView == AddUserTableView) {
            return AddUser.connectedUsers.count;
        } else {
            let displayCount = min( AddUser.dropdownUsers.count, 3 );
            self.dropdownTableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: CGFloat(CGFloat(displayCount) * tableView.rowHeight));
            return AddUser.dropdownUsers.count;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.item;
        if ( tableView == AddUserTableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addusercell", for: indexPath) as! addusercell;
            let firstname = AddUser.connectedUsers[row].FirstName
            let lastname = AddUser.connectedUsers[row].LastName
            if (AddUser.connectedUsers[row].Type == "docter") {
                cell.docterLabel.isHidden = false;
            }else{
                cell.docterLabel.isHidden = true;
            }
            cell.name.text = "\(firstname) \(lastname)";
            
            cell.buttonAction = { (sender) in
                if (AddUser.connectedUsers.count > 0) {
                    Network.RemovePermission(
                        patientID: AddUser.connectedUsers[row].PatientID,
                        userId: AddUser.connectedUsers[row].UserID,
                        urlFail: {
                            Network.netAlert()
                        },
                        comp: { (json, error, response) in
                            AddUser.connectedUsers.remove(at: row)
                            self.AddUserTableView.reloadData()
                        },
                        fail: { error in
                            Network.netAlert()
                    })
                }
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dropdowncell", for: indexPath) as! dropdowncell;
            let firstname = AddUser.dropdownUsers[row].firstName
            let lastname = AddUser.dropdownUsers[row].lastName
            if (AddUser.dropdownUsers[row].type == "docter") {
                cell.docterLabel.isHidden = false;
            }else{
                cell.docterLabel.isHidden = true;
            }
            cell.name.text = "\(firstname ?? "") \(lastname ?? "")";
            return cell;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.item;
        if ( tableView == AddUserTableView) {
            let account = self.storyboard?.instantiateViewController(withIdentifier: "AddedUserAccountInfo") as! AddedUserAccountInfo;

            account.user = AddUser.connectedUsers[row];
            account.userPatient = AddUser.connectedUsers[row].PatientID;
            Navigation.GoTopWithController(view: account, anim: true);
        } else {
            self.AddUserText.text = AddUser.dropdownUsers[row].username;
            AddUser.dropdownUsers = [UserData]()
            self.dropdownTableView.reloadData()
            self.TextChanged(self);
        }

    }
}



class AddUser: UIViewController {
    
    /// The added users table view
    @IBOutlet weak var AddUserTableView: UITableView!
    
    /// The text for the added users
    @IBOutlet weak var AddUserText: UITextField!
    
    /// spin my world right round...
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// Quick search users
    @IBOutlet weak var dropdownTableView: UITableView!
    
    
    /// found users
    private static var dropdownUsers: [UserData] = [UserData]();
    
    /// users that are added
    private static var connectedUsers: [UserDataConnected] = [UserDataConnected]();
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "cell"

    // don't forget to hook this up from the storyboard
    @objc func updateList(){
        self.AddUserTableView.reloadData()
    }
    
    @IBAction func enableDropdown(_ sender: Any) {
        self.dropdownTableView.isHidden = false;
    }
    
    @IBAction func disableDropdown(_ sender: Any) {
        self.dropdownTableView.isHidden = true;
    }
    
    @IBAction func TextEdited(_ sender: Any) {
        if(AddUserText?.text ?? "" == ""){
            self.dropdownTableView.isHidden = true;
        }
        if AddUserText?.text?.count ?? 0 > 3 {
            self.activityIndicator.isHidden = false;
            self.dropdownTableView.isHidden = false;
                Network.GetUsersFromName(
                    username: AddUserText?.text ?? "",
                    urlFail: {
                        self.activityIndicator.isHidden = true
                    },
                    comp: { (json, error, response) in
                        if response.response?.statusCode == 200 {
                            do {
                                if let json = json {
                                    AddUser.dropdownUsers = try JSONDecoder().decode([UserData].self, from: Data(json.utf8) )
                                    self.dropdownTableView.reloadData()
                                    
                                }
                                
                            }catch{
                                AddUser.dropdownUsers = [UserData]()
                                self.dropdownTableView.reloadData()
                            }
                        }
                        self.activityIndicator.isHidden = true
                    },
                    fail: { (error) in
                        AddUser.dropdownUsers = [UserData]()
                        self.dropdownTableView.reloadData()
                        self.activityIndicator.isHidden = true
                        
                })
        }else {
            self.dropdownTableView.isHidden = true;
        }
    }
    
    @IBAction func TextChanged(_ sender: Any) {
        self.activityIndicator.isHidden = false;
        self.dropdownTableView.isHidden = true;
        Network.GetUserByName(
            username: self.AddUserText.text ?? "",
            urlFail: {
            },
            comp: {(json, error, response) in
                if response.response?.statusCode == 200 {
                    if let json = json {
                        do {
                            let user = try JSONDecoder().decode(UserData.self, from: Data(json.utf8) )
                            Network.CreatePermission(
                                userid: user.userID ?? 0,
                                urlFail: {
                                    self.activityIndicator.isHidden = true;
                                },
                                comp: { (json, error, response) in
                                    
                                    if response.response?.statusCode == 200 {
                                        do {
                                            Network.GetUsersConnectedPatients(
                                                urlFail: {
                                                
                                                },comp: {(json, error, response) in
                                                    if let json = json{
                                                        do {
                                                            AddUser.connectedUsers = try JSONDecoder().decode([UserDataConnected].self, from: Data(json.utf8) )
                                                            self.activityIndicator.isHidden = true;
                                                            self.AddUserTableView.reloadData()
                                                        }catch{
                                                            CustomAlert.Show(
                                                                title: "Waarschuwing",
                                                                msg: "Er was een probleem bij het toevoegen van de gebruiker. Probeer het later opniew.",
                                                                h1Text: "Close",
                                                                h1: nil
                                                            )
                                                        }
                                                    }
                                                }, fail: { error in
                                                    Network.netAlert()
                                                })
                                        }
                                    } else {
                                        Network.netAlert()
                                    }
                                },
                                fail: { error in
                                    self.activityIndicator.isHidden = true;
                                    CustomAlert.Show(
                                        title: NSLocalizedString("Warning", comment: ""),
                                        msg: NSLocalizedString("ErrorRecPatient", comment: ""),
                                        h1Text: NSLocalizedString("Close", comment: ""),
                                        h1: nil
                                    )
                                })
                        }catch{
                            self.activityIndicator.isHidden = true;
                        }
                    }
                }
            }, fail: { error in
                self.activityIndicator.isHidden = true;
            })
    }
    
    @objc func handleDismiss() {
        
        if AddUserTableView != nil {
            Network.GetUsersConnectedPatients(
                urlFail: {
                    Network.netAlert()
                },comp: {(json, error, response) in
                    if let json = json{
                        do {
                            AddUser.connectedUsers = try JSONDecoder().decode([UserDataConnected].self, from: Data(json.utf8) )
                            self.AddUserTableView.reloadData()
                            AddUser.dropdownUsers = [UserData]()
                            self.dropdownTableView.reloadData()
                            self.dropdownTableView.isHidden = true;
                        }catch{
                            CustomAlert.Show(
                                title: "Waarschuwing",
                                msg: "Er was een probleem met het ophalen van de verbonden gebruikers",
                                h1Text: "Close",
                                h1: nil
                            )
                        }
                    }
                }, fail: { error in
                    Network.netAlert()
            })
        }
    }
    
    @objc func tap(gesture: UITapGestureRecognizer) {
        if gesture.state == .ended{
            self.dropdownTableView.isHidden = true;
        }
        
    }
    
    func initialize() {
        AddUser.dropdownUsers = [UserData]()
        self.dropdownTableView.reloadData()
        
        let notificationNme = NSNotification.Name("NotificationIdf")
        NotificationCenter.default.addObserver(self, selector: #selector(handleDismiss), name: notificationNme, object: nil);
        

            Network.GetUsersConnectedPatients(
                urlFail: {
                    Network.netAlert()
                },comp: {(json, error, response) in
                    if let json = json{
                        do {
                            AddUser.connectedUsers = try JSONDecoder().decode([UserDataConnected].self, from: Data(json.utf8) )
                            self.activityIndicator.isHidden = true;
                            self.AddUserTableView.reloadData()
                        }catch{
                            CustomAlert.Show(
                                title: "Waarschuwing",
                                msg: "Er was een probleem met het ophalen van de verbonden gebruikers",
                                h1Text: "Close",
                                h1: nil
                            )
                        }
                    }
                }, fail: { error in
                    Network.netAlert()
            })
            self.AddUserTableView.delegate = self
            self.AddUserTableView.dataSource = self
            self.dropdownTableView.isHidden = true;
            self.dropdownTableView.delegate = self;
            self.dropdownTableView.dataSource = self;
            self.dropdownTableView.register(UINib(nibName: "dropdowncell", bundle: nil), forCellReuseIdentifier: "dropdowncell");
            self.AddUserTableView.register(UINib(nibName: "addusercell", bundle: nil), forCellReuseIdentifier: "addusercell");
            self.AddUserTableView.separatorStyle = .none;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize();
        }
    
}


