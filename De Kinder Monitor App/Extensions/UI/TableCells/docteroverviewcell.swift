//
//  docteroverviewcell.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 25/01/2020.
//  Copyright © 2020 FixIt. All rights reserved.
//

import Foundation
import UIKit

/// Custome docter overview cell
final class docteroverviewcell: UITableViewCell {
    
    /// Pattient name label
    @IBOutlet weak var patientName: UILabel!
    
    
    /// Patient heartrate label
    @IBOutlet weak var patientHeartRate: UILabel!
    
    /// Patient temp label
    @IBOutlet weak var patientTemp: UILabel!
    
    /// Patient freq label
    @IBOutlet weak var patientFreq: UILabel!
    
    /// Patient saturation label
    @IBOutlet weak var patientSat: UILabel!
    
    /// Freq limit label
    @IBOutlet weak var freqLimitLabel: UILabel!
    
    /// Heart limit label
    @IBOutlet weak var heartLimitLabel: UILabel!
    
    /// Temp limit label
    @IBOutlet weak var tempLimitLabel: UILabel!
    
    /// Saturation limit label
    @IBOutlet weak var satLimitLabel: UILabel!
    
    /// The heart icon
    @IBOutlet weak var heartIcon: UIImageView!
    
    /// The saturation icon
    @IBOutlet weak var satIcon: UIImageView!
    
    /// The frequancy icon
    @IBOutlet weak var freqIcon: UIImageView!
    
    /// The temperature icon
    @IBOutlet weak var tempIcon: UIImageView!
    
    /// the error icon
    @IBOutlet weak var errorIcon: UIImageView!
    
    /// The time the error occured
    @IBOutlet weak var errorTime: UILabel!
    
    /// Date formatter
    private var dateFormatter: DateFormatter?
    
    /// normal func
    override func awakeFromNib() {
        frame.origin.y += 32
        patientName.text = NSLocalizedString("ErrorRecPatient", comment: "");
    }
    
    /// normal func
    override func prepareForReuse() {
        if(patientName != nil) {
            patientName.text = ""
        }
        if(freqLimitLabel != nil) {
            freqLimitLabel.text = "E"
        }
        if(heartLimitLabel != nil) {
            heartLimitLabel.text = "E"
        }
        if(tempLimitLabel != nil) {
            tempLimitLabel.text = "E"
        }
        if(satLimitLabel != nil) {
            satLimitLabel.text = "E"
        }
        heartIcon.tintColor = Colors.Red;
        satIcon.tintColor = Colors.Red;
        freqIcon.tintColor = Colors.Red;
        tempIcon.tintColor = Colors.Red;
        patientName.textColor = Colors.Red;
        errorIcon.isHidden = false;
        dateFormatter = DateFormatter()
        dateFormatter?.dateFormat = "HH:mm";
        errorTime.text = self.dateFormatter?.string(from: Date());
    }
}
