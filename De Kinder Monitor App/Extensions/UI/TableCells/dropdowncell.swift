//
//  dropdowncell.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 27/01/2020.
//  Copyright © 2020 FixIt. All rights reserved.
//

import Foundation
import UIKit

final class dropdowncell: UITableViewCell {
    
    /// the name of the user
    @IBOutlet weak var name: UILabel!
    
    /// If the user is a docter or not
    @IBOutlet weak var docterLabel: UILabel!
    
    override func awakeFromNib() {
        name.text = NSLocalizedString("FirSurPlaceholder", comment: "");
    }
    override func prepareForReuse() {
        name.text = NSLocalizedString("FirSurPlaceholder", comment: "");
    }
    
}
