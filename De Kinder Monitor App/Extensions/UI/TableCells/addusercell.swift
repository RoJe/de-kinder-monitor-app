//
//  addusercell.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 27/01/2020.
//  Copyright © 2020 FixIt. All rights reserved.
//

import Foundation
import UIKit

final class addusercell: UITableViewCell {
    
    /// The user his name
    @IBOutlet weak var name: UILabel!
    
    /// The docter label to indicate if the user is a docter
    @IBOutlet weak var docterLabel: UILabel!
    
    /// The button to move to trash
    @IBOutlet weak var button: UIButton!
    
    /// normal klick action
    var buttonAction: ((_ sender: AnyObject) -> Void)?
    
    
    /// When the delete is pressed
    /// - Parameter sender: the button that has been pressed
    @IBAction func DeletePressed(_ sender: UIButton) {
        self.buttonAction?(sender)
    }
    
    /// Normal func
    override func awakeFromNib() {
        name.text = NSLocalizedString("FirSurPlaceholder", comment: "");
    }
    
    /// Normal func
    override func prepareForReuse() {
        name.text = NSLocalizedString("FirSurPlaceholder", comment: "");
    }
    
}
