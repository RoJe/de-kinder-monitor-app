//
//  LineChartsTestst.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 1/25/20.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit
import Charts


/// class to add stuff to the chart
class LineChartTests: LineChartView
{
    var dataSet: LineChartDataSet!
    
    func setChart(dataPoints: [String], values: [Double]) {
        let chartFormatter = IndexAxisValueFormatter(values: dataPoints);
        self.xAxis.valueFormatter = chartFormatter
        
        self.noDataText = NSLocalizedString("NoLineData", comment: "");
        
        var entries: [ChartDataEntry] = Array()
        
        for (i, value) in values.enumerated()
        {
            entries.append(ChartDataEntry(x: Double(i), y: value, icon: UIImage(named: "icon", in: Bundle(for: self.classForCoder), compatibleWith: nil)))
        }
        dataSet = LineChartDataSet(entries: entries, label: NSLocalizedString("GraphName", comment: ""))
        dataSet.drawCirclesEnabled = false
        dataSet.drawIconsEnabled = false
        dataSet.iconsOffset = CGPoint(x: 0, y: 20.0)

        self.backgroundColor = NSUIColor.clear
        self.leftAxis.axisMinimum = 0.0
        self.rightAxis.axisMinimum = 0.0
        self.data = LineChartData(dataSet: dataSet)
        self.notifyDataSetChanged();
    }
}
