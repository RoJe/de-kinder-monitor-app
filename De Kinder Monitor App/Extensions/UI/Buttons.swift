//
//  roundedButton.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 08/12/2019.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome


/// Button for frimary action we user
class PrimaryButton: UIButton {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 8;
    }
}

/// Button for secondary action we use
class SecondaryButton: UIButton {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 8;
        self.layer.borderColor = UIColor.blue.cgColor;
        self.layer.borderWidth = 1;
        self.tintColor = UIColor.systemGray;
    }
}

/// Mac like rounded button
class MacRounded: UIButton {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 25;
        self.layer.borderColor = UIColor.systemGray.cgColor;
        self.layer.borderWidth = 1;
        self.tintColor = UIColor.systemGray;
    }
}

/// Picker button for the detail views
class PickerButton: UIButton {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderWidth = 1;
        self.layer.borderColor = UIColor.systemGray.cgColor;
    }
}
