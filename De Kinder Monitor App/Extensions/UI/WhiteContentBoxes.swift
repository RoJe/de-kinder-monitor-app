//
//  WhiteContentBoxes.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 09/12/2019.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit


/// A view with a shadow
class WhiteContentBoxPressable: UIView {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 8;
    }
    
    override func layoutSubviews() {
        dropShadow()
    }


    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
    }
    
    func onPress() {
        self.layer.backgroundColor = UIColor.systemGray.cgColor;
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.00 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    
    @IBInspectable var imageShadowColor: UIColor = .black {
        didSet {
            dropShadow()
        }
    }
    @IBInspectable var imageShadowOpacity: Float = 0.0 {
        didSet {
            dropShadow()
        }
    }
    @IBInspectable var imageShadowRadius: CGFloat = 0.0 {
        didSet {
            dropShadow()
        }
    }
    @IBInspectable var imageShadowOffset: CGSize = .zero {
        didSet {
            dropShadow()
        }
    }
    fileprivate func dropShadow() {
        self.layer.shadowColor = imageShadowColor.cgColor
        self.layer.shadowOpacity = imageShadowOpacity
        self.layer.shadowOffset = imageShadowOffset
        self.layer.shadowRadius = imageShadowRadius
        self.layer.shadowPath = UIBezierPath(roundedRect: self.layer.bounds, cornerRadius: cornerRadius).cgPath
    }
}

class WhiteContentBox: UIView {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 8;
    }
}
