//
//  Network_Base.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 1/19/20.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import Alamofire


extension Network {
    /// Perform a get request
    /// - Parameters:
    ///   - url: url description
    ///   - headers: required headers
    ///   - parameters: required parameters
    ///   - completion: completion function
    static func Get( url: URL, headers: HTTPHeaders?, _ parameters: [String?:Any?]? = [nil:nil], completion: response ){
        Alamofire.request(
                            url,
                            method: .get,
                            parameters: parameters as? Parameters,
                            headers: headers
        ).validate()
        .response { response in
            if let error = response.error{
                completion?(nil, error, response)
                return
            }
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8){
                completion?(utf8Text, nil, response)
                return
            }
            completion?(nil, nil, response)
        }
    }
    
    // Perform a post request
    /// - Parameters:
    ///   - url: url description
    ///   - parameters: required parameters
    ///   - completion: completion function
    static func Post( url: URL, headers: HTTPHeaders?, parameters: [String:Any], completion: response){
        Alamofire.request(
                            url,
                            method: .post,
                            parameters: parameters,
                            encoding: JSONEncoding.default,
                            headers: headers
            ).validate()
            .response{ response in
            
            if let error = response.error{
                completion?(nil, error, response)
                return
            }
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8){
                completion?(utf8Text, nil, response)
                return
            }
            
            completion?(nil, nil, response)
        }
    }
    
    // Perform a put request
    /// - Parameters:
    ///   - url: url description
    ///   - headers: required headers
    ///   - parameters: required parameters
    ///   - completion: completion function
    static func Put( url: URL, headers: HTTPHeaders?, parameters: [String:Any], completion: response ){

        Alamofire.request(
                            url,
                            method: .put,
                            parameters: parameters,
                            encoding: JSONEncoding.default,
                            headers: headers
        ).validate()
        .response { response in
            if let error = response.error{
                completion?(nil, error, response)
                return
            }
            completion?(nil, nil, response)
        }
    }
    
    // Perform a delete request
    /// - Parameters:
    ///   - url: url description
    ///   - headers: required headers
    ///   - completion: completion function
    static func Delete( url: URL, headers: HTTPHeaders?, completion: response ){
        Alamofire.request(
                            url,
                            method: .delete,
                            headers: headers
        ).validate()
        .response { response in
            
            if let error = response.error{
                completion?(nil, error, response)
                return
            }
            
            completion?(nil, nil, response)
        }
    }
}
