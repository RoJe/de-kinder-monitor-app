//
//  string.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/8/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation


extension String {
    
    /// Does the string have any special charater?
    var containsSpecialCharacter: Bool {
      let regex = ".*[^A-Za-z0-9].*"
      let str = NSPredicate(format:"SELF MATCHES %@", regex)
      return str.evaluate(with: self)
    }
    
    /// Does the string contain an uppercase?
    var containsUppercase: Bool {
      let regex = ".*[A-Z].*"
      let str = NSPredicate(format:"SELF MATCHES %@", regex)
      return str.evaluate(with: self)
    }
    
    /// Does the string use an lowercase?
    var containsLowercase: Bool {
      let regex = ".*[a-z].*"
      let str = NSPredicate(format:"SELF MATCHES %@", regex)
      return str.evaluate(with: self)
    }
    
    /// Does the string contain an digit?
    var containsDigit: Bool {
      let regex = ".*[0-9].*"
      let str = NSPredicate(format:"SELF MATCHES %@", regex)
      return str.evaluate(with: self)
    }
    
    /// Did the user full in an correct day of birth?
    var CorrectBirthday : Bool {
        let regex = "^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$"
        let str = NSPredicate(format:"SELF MATCHES %@", regex)
        return str.evaluate(with: self)
    }
    
    var CorrectEmail : Bool {
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let str = NSPredicate(format:"SELF MATCHES %@", regex)
        return str.evaluate(with: self)
    }
    
    /// Did the user fill in a correct password?
    var CorrectPassword : Bool{
        let regex = ".*[^A-Za-z0-9].*"
        let str = NSPredicate(format:"SELF MATCHES %@", regex)
        return str.evaluate(with: self)
    }
    
    /// Did the user fill in a correct child code?
    var CorrectChildCode: Bool{
        let regex = "[a-zA-Z0-9]{8}\\-[a-zA-Z0-9]{4}\\-[a-zA-Z0-9]{4}\\-[a-zA-Z0-9]{4}\\-[a-zA-Z0-9]{12}"
        let str = NSPredicate(format:"SELF MATCHES %@", regex)
        return str.evaluate(with: self)
    }
}
