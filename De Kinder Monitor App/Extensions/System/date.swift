//
//  date.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 1/21/20.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
// Some date extensions to make live easier
extension Date {
    
    /// format a date
    /// - Parameter format: wanted format
    func string(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    /// add/substract minutes from a date
    /// - Parameter minutes: minutes to add/substract
    func adding(minutes: Int) -> Date {
        
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    /// add/substract hours from a date
    /// - Parameter hours: hours to add/substract
    func adding(hours: Int) -> Date {
        
        return Calendar.current.date(byAdding: .hour, value: hours, to: self)!
    }
    
    /// add/substract days from a date
    /// - Parameter days: days to add/substract
    func adding(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
    /// add/substract months from a date
    /// - Parameter months: months to add/substract
    func adding(months: Int) -> Date {
        return Calendar.current.date(byAdding: .month, value: months, to: self)!
    }
}
