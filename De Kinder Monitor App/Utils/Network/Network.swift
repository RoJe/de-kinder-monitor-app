//
//  Network.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/8/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation
import Alamofire
import Starscream


/// Struct to save the login token from the API
struct userToken : Decodable {
    var `token` : String
}

/// struct to save all the user data from the API
struct UserData: Decodable, Equatable {
    var `userID`: Int?
    var `username`: String?
    var `password`: String?
    var `firstName`: String?
    var `lastName`: String?
    var `phoneNumber`: String?
    var `email`: String?
    var `type`: String?
}

// struct to save all the patient data from the API
struct PatientData: Decodable, Equatable {
    var `patientID`: Int
    var `firstname`: String
    var `lastname`: String
    var `birthday`: String
}

// struct to save all the permission data from the API
struct PermissionsData: Decodable, Equatable {
    var `permissionID`: Int?
    var `userID`: Int?
    var `patientID`: Int?
    var `write`: Bool?
}

// struct to save all the users connected to a patient
struct UserDataConnected: Decodable {
    var `PermissionID`: Int
    var `PatientID`: Int
    var `UserID`: Int
    var `Username`: String
    var `FirstName`: String
    var `LastName`: String
    var `PhoneNumber`: String
    var `Email`: String
    var `Type`: String?
}

/// Struct for the API sensor data structure
struct SensorApiData: Decodable, Equatable {
    var `sensorID`: Int
    var `patientID`: Int
    var `type`: String
    var `brand`: String
    var `thresholdMin`: Int
    var `thresholdMax`: Int
    var `preSharedKey`: String
    var `inUseSince`: String
}


/// the json socket response
struct SensorSocketData: Decodable{
    var `PatientID`: String
    var `Heart`: String
    var `Temp`: String
    var `Freq`: String
    var `Sat`: String
    var `TimeSend`: String
}

/// struct for graph points
struct GraphPoint: Decodable {
    var `measurementID`: Int
    var `sensorID`: Int
    var `value`: Int
    var `time`: String
}

/// custom struct to get all the graphpoints form the api
struct GraphPointSimple: Decodable {
    var `value`: [Double]
    var `time`: [String]
}

/// Struct for an error
struct NetError: Decodable{
    var `error`: String
}


/// Class containing all the network calls
class Network {
    /// Normal function structure for arguments
    typealias normalAction = (() -> Void)?
    
    /// Normal function structure for arguments with error
    typealias errorAction = ((Error?) -> Void)?
    
    /// Normal function structure for arguments with string
    typealias stringAction = ((String?) -> Void)?
    
    /// Normal function structure for arguments with data
    typealias dataAction = ((Data?) -> Void)?
    
    /// Normal function structure for arguments with all the data needed
    typealias response = ((String?, Error?, DefaultDataResponse) -> Void)?
    
    /// url to the api
    private static var HTTPPullUrl = "https://ehealthapi2r-fun.azurewebsites.net/api/";
    
    /// url to the socket vm
    private static var SocketUrl = "http://51.136.23.80:7071/api/";
    
    
    static var activeSocket : cWebsocket?
    
    
    /// Function to create a socket connection
    /// - Parameters:
    ///   - url: url
    ///   - onConnect: function
    ///   - onDisconnect: function
    ///   - onText: function
    ///   - onData: function
    static func CreateSocketConnection(url: String, onConnect: normalAction, onDisconnect: errorAction, onText: stringAction, onData: dataAction){
        if(self.activeSocket != nil && self.activeSocket!.isConnected()){
            return
        }
        self.activeSocket = cWebsocket(url: url, onConnect: onConnect, onDisconnect: onDisconnect, onText: onText, onData: onData)
    }
    
    /// Return the active socket connection
    static func GetSocketConnection() -> cWebsocket?{
        if Network.activeSocket == nil {
            return nil
        }
        return self.activeSocket
    }
    
    /// Disconnect the current active socket
    static func DisconnectSocket(){
        self.activeSocket?.disconnect()
    }
    
    /// Function to check if the user is still loged in.
    /// - Parameters:
    ///   - urlFailure: action to execute if the url doesn't work
    ///   - completion: action to execute if the request was succesfull
    ///   - failure: action to execute if the request was a failure
    public static func IsUserValid(urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/UserStillValid") else {
            urlFail?()
            return
        }
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    /// Function to get the sensor data from a specific sensor
    /// - Parameters:
    ///   - name: name of the sensor
    ///   - patientId: the id of the patient
    ///   - from: from time
    ///   - to: to time
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    static func GetSensorData(name: String, patientId: Int, from: Date, to: Date, urlFail: normalAction, comp: response, fail: errorAction){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
        let newFrom = dateFormatter.string(from: from).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "" // Format the from date
        let newTo = dateFormatter.string(from: to).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "" // Format the to date
        guard let urlToExecute = URL(string: HTTPPullUrl + "Sensors/FromName/\(name)/FromPatient/\(patientId)/measurements?from=\(newFrom)&to=\(newTo)") else {
            urlFail?()
            return
        }
        
        Network.Get(
            url: urlToExecute,
            headers: [
                "Authorization" : "Bearer \(User.GetAuthToken() ?? "")",
                "Accept": "application/json"
            ],completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                }
                comp?(json, error, response)
        })
    }
    
    
    /// Function to update a given sensor
    /// - Parameters:
    ///   - sensor: The new sensor data
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    public static func UpdateSensorData(sensor: SensorApiData, urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Sensors/updateSensor") else {
            urlFail?()
            return
        }
        Network.Put(
            url: urlToExecute,
            headers:  [
                "Authorization" : "Bearer \(User.GetAuthToken() ?? "")",
                "Accept": "application/json"
            ],
            parameters: [
                "SensorID": sensor.sensorID,
                "PatientID": sensor.patientID,
                "Type": sensor.type,
                "Brand": sensor.brand,
                "ThresholdMin": sensor.thresholdMin,
                "ThresholdMax": sensor.thresholdMax,
                "PreSharedKey": sensor.preSharedKey,
                "InUseSince": sensor.inUseSince
            ], completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    /// Function to  get a user by name
    /// - Parameters:
    ///   - username: the name of the user
    ///   - urlFailure: action to execute if the url doesn't work
    ///   - completion: action to execute if the request was succesfull
    ///   - failure: action to execute if the request was a failure
    public static func GetUserByName(username: String, urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/ByName/\(username)") else {
            urlFail?()
            return
        }
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
           })
    }
    
    /// Function to  register a user
    /// - Parameters:
    ///   - user: the user
    ///   - urlFailure: action to execute if the url doesn't work
    ///   - completion: action to execute if the request was succesfull
    ///   - failure: action to execute if the request was a failure
    static func Register(user: UserData, urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users") else {
            urlFail?()
            return
        }
        Network.Post(url: urlToExecute, headers: nil, parameters: [
            "Userid": 0,
            "Username": user.username ?? "N/A",
            "Password": user.password ?? "N/A",
            "FirstName": user.firstName ?? "N/A",
            "LastName": user.lastName ?? "N/A",
            "PhoneNumber": user.phoneNumber ?? "N/A",
            "Email": user.email ?? "N/A",
            "Type": "User",
            ], completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    
    /// Function to check if a username doesn't already exsist
    /// - Parameters:
    ///   - username: teh username
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    static func RegisterCheckUserName(username: String, urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users") else {
            urlFail?()
            return
        }
        Network.Post(url: urlToExecute, headers: nil, parameters: [
            "username": username,
            ], completion: {(json, error, response) in
                comp?(json, error, response)
        })
    }
    
    
    /// Function to register a new administrator user
    /// - Parameters:
    ///   - PreSharedKey: the key assosiated with the child
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    static func RegisterUserWithChild(PreSharedKey: String?, urlFail: normalAction, comp: response, fail: errorAction) {
        guard PreSharedKey != nil else {
            urlFail?()
            return;
        }
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/me/Link/Patient") else {
            urlFail?()
            return
        }
        
        Network.Post(
            url: urlToExecute,
            headers:  [
                "Authorization" : "Bearer \(User.GetAuthToken() ?? "")",
                "Accept": "application/json"
            ],
            parameters: [
            "PreSharedKey": PreSharedKey as Any,
            ],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    
    /// Function to  get the current user
    /// - Parameters:
    ///   - urlFailure: action to execute if the url doesn't work
    ///   - completion: action to execute if the request was succesfull
    ///   - failure: action to execute if the request was a failure
    static func GetCurrentUser(urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/me") else {
            urlFail?()
            return
        }
        
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    
    /// Function to update the user his password
    /// - Parameters:
    ///   - password: the password
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    static func UpdateUserPassword(password: String, urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/me") else {
            urlFail?()
            return
        }
        let userData = User.GetData();
        Network.Put(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            parameters: [
                "Userid": 0,
                "Username": userData?.username ?? "N/A",
                "Password": password,
                "FirstName": userData?.firstName ?? "N/A",
                "LastName": userData?.lastName ?? "N/A",
                "PhoneNumber": userData?.phoneNumber ?? "N/A",
                "Email": userData?.email ?? "N/A",
            ],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    /// Function to  get create a permission to a patient and a user
    /// - Parameters:
    ///   - userid: The id of the user
    ///   - urlFailure: action to execute if the url doesn't work
    ///   - completion: action to execute if the request was succesfull
    ///   - failure: action to execute if the request was a failure
    static func CreatePermission(userid: Int, urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/me/Patients/Permissions") else {
            urlFail?()
            return
        }
        Network.getUserPermissions(
            urlFail: {
        }, comp: { (json, error, response) in
            do {
                if response.response?.statusCode == 200 {
                    let permission = try JSONDecoder().decode(Array<PermissionsData>.self, from: Data(json!.utf8) )
                    if(permission[0].patientID != nil && permission[0].write != nil && permission[0].write == true) {
                        Network.Post(
                            url: urlToExecute,
                            headers:  [
                                "Authorization" : "Bearer \(User.GetAuthToken() ?? "")",
                                "Accept": "application/json"
                            ],
                            parameters: [
                                "PermissionId": 0,
                                "UserId": userid,
                                "PatientId": permission[0].patientID as Any,
                                "Write": false as Any
                            ],
                            completion: {(json, error, response) in
                                if let error = error {
                                    fail?(error)
                                    return
                                }
                            comp?(json, error, response)
                        })
                    }

                } else {

                }
            } catch{

            }
        }, fail: { error in
            //If the fail is called a 404 has occured, there is no permission data available, show empty homescreen.
        })
    }
    
    
    /// Get the sensors connected to a patient
    /// - Parameters:
    ///   - patientID: the id of the patient
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    static func GetSensors(patientID: Int, urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Patients/\(patientID)/Sensors") else {
            urlFail?()
            return
        }
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
           })
    }
    
    /// Function to  get all the patients the user is connected to
    /// - Parameters:
    ///   - urlFailure: action to execute if the url doesn't work
    ///   - completion: action to execute if the request was succesfull
    ///   - failure: action to execute if the request was a failure
    static func GetConnectedPatients(urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/me/Patients") else {
            urlFail?()
            return
        }
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
           })
    }
    
    /// Function to  get all the users connected to the patients we're connected to
    /// - Parameters:
    ///   - urlFailure: action to execute if the url doesn't work
    ///   - completion: action to execute if the request was succesfull
    ///   - failure: action to execute if the request was a failure
    static func GetUsersConnectedPatients(urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/me/ConnectedToPatients") else {
            urlFail?()
            return
        }
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
           })
    }
    
    
    /// Function to remove permissions from a specific user
    /// - Parameters:
    ///   - patientID: the id of the patient
    ///   - userId: the id of the user
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    static func RemovePermission(patientID: Int, userId: Int, urlFail: normalAction, comp: response, fail: errorAction) {
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/me/Patients/\(patientID)/Permissions/\(userId)") else {
            urlFail?()
            return
        }
    
    
        Network.Delete(
            url: urlToExecute,
            headers:  [
                "Authorization" : "Bearer \(User.GetAuthToken() ?? "")",
                "Accept": "application/json"
        ], completion: {(json, error, response) in
            if let error = error {
                fail?(error)
                return
            }
            comp?(json, error, response)
        })
    }
    
    /// Function to get the users from their name
    /// - Parameters:
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    static func GetUsersFromName(username: String, urlFail: normalAction, comp: response, fail: errorAction) {
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/Like/\(username)") else {
            urlFail?()
            return
        }
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    /// Function to get the user his permission
    /// - Parameters:
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    static func getUserPermissions(urlFail: normalAction, comp: response, fail: errorAction) {
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/me/Permissions") else {
            urlFail?()
            return
        }
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    
    /// Function to login the user
    /// - Parameters:
    ///   - username: username
    ///   - password: password
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    static func loginUser(username: String, password: String, urlFail: normalAction, comp: response, fail: errorAction) {
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/Login") else {
            urlFail?()
            return
        }
        Network.Post(url: urlToExecute, headers: nil, parameters: [
            "Userid": 0,
            "Username": username,
            "Password": password,
            ], completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    /// Function to check the user his username
    /// - Parameters:
    ///   - username: username
    ///   - urlFail: function
    ///   - comp: function
    ///   - fail: function
    static func checkUserName(username: String, urlFail: normalAction, comp: response, fail: errorAction) {
        guard let urlToExecute = URL(string: HTTPPullUrl + "Users/Exists/" + username) else {
            urlFail?()
            return
        }
        
        Network.Get(url: urlToExecute, headers: nil, [
            "Username": username,
            ], completion: {(json, error, response) in
                comp?(json, error, response)
        })
    }
    
    /// Function to throw an alert
    static func netAlert() {
        CustomAlert.Show(
            title: NSLocalizedString("Warning", comment: ""),
            msg: NSLocalizedString("NoServerCon", comment: ""),
            h1Text: NSLocalizedString("Close", comment: ""),
            h1: nil
        )
    }
}
