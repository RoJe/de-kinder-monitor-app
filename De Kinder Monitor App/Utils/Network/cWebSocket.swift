//
//  WebSocket.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/31/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation
import Starscream

/// WEBSOCKETS!
class cWebsocket : WebSocketDelegate {
    
    /// Normal function structure for arguments
    typealias normalAction = (() -> Void)?
    
    /// Normal function structure for arguments with error
    typealias errorAction = ((Error?) -> Void)?
    
    /// Normal function structure for arguments with string
    typealias stringAction = ((String?) -> Void)?
    
    /// Normal function structure for arguments with data
    typealias dataAction = ((Data?) -> Void)?
    
    
    /// The url the socket connects to
    private var url: String
    
    /// Normal function structure for arguments
    private var socketConnectFunc: (() -> Void)?
    
    /// Normal function structure for arguments with error
    private var socketDisconnectFunc: ((Error?) -> Void)?
    
    /// Normal function structure for arguments with string
    private var socketTextFunc: ((String?) -> Void)?
    
    /// Normal function structure for arguments with data
    private var socketDataFunc: ((Data?) -> Void)?
    
    
    /// The socket
    private var socket : WebSocket?
    
    
    /// The amount the socket tried to reconnect
    private var reconnectAttempts: Int
    
    
    /// Time it takes for the socket to reconnect
    private var socketReconnectTime: Double
    
    
    init(url: String, onConnect: normalAction, onDisconnect: errorAction, onText: stringAction, onData: dataAction){
        self.url = url
        self.socketConnectFunc = onConnect              // Save the function for customization
        self.socketDisconnectFunc = onDisconnect        // Save the function for customization
        self.socketTextFunc = onText                    // Save the function for customization
        self.socketDataFunc = onData                    // Save the function for customization
        self.reconnectAttempts = 0                      // Has not tried to reconnect
        self.socketReconnectTime = 2                    // Reconnect every 2 seconds
        
        
        self.createSocket()                             // Create the socket
    }
    
    
    
    /// Send text
    /// - Parameter msg: text
    func SendText(msg: String) -> Bool{
        if self.socket == nil || !self.socket!.isConnected { // Do we actually have a socket? and is it connected?
            return false    // Seems not
        }
        self.socket!.write(string: msg) // Send the text
        return true // We did it!
    }
    
    /// Create the socket and set everything
    private func createSocket(){
        guard let url = URL(string: self.url) else { // Check if the url can actually be reached
            return // Woops
        }
        var request = URLRequest(url: url) // Check the url
        request.timeoutInterval = 5 // Connection may timeout every 5 seconds
        self.socket = WebSocket(request: request) // Create the socket
        self.socket?.enableCompression = false // No compression here. Sockets are a b**!@# with encryption
        self.socket?.disableSSLCertValidation = true // No need for this
        self.socket?.delegate = self // Set the delegate to this class
        self.socket?.onConnect = self.socketConnectFunc // Perform the custom connect func when we connect
        self.socket?.onDisconnect = self.socketDisconnectFunc // Perform the custom disconnect func when we connect
        self.socket?.onText = self.socketTextFunc // Perform the custom text func when we connect
        self.socket?.onData = self.socketDataFunc // Perform the custom data func when we connect
        self.socket?.connect() // Connect the socket
        
        if self.socket?.isConnected ?? false { // Did it actually connect?
            self.reconnectAttempts = 0 // Reset this counter
        }
    }
    
    /// Disconnect socket
    public func disconnect(){
        if self.socket == nil { // Do we still have the socket? Protection for multithreading
            return
        }
        Sensor.setActive(tempActive: false); // Socket is no more
        self.socket!.disconnect() // disconnect the socket
        self.socket = nil // soo.. its garbage now?
    }
    
    /// Handler for when the socket connect
    /// - Parameter socket: the socket
    func websocketDidConnect(socket: WebSocketClient) {
        Sensor.setActive(tempActive: true); // It is active now
    }
    
    /// Is the socket connected?
    func isConnected() -> Bool{
        return ( self.socket == nil ? false :  self.socket!.isConnected ) // Is the socket still connected?
    }
    
    /// Did the socket disconnect?
    /// - Parameters:
    ///   - socket: The socket
    ///   - error: The possible error
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        
        if self.reconnectAttempts >= 50 { // did we go over our reconnect limits? alert the user.
            Sensor.setActive(tempActive: false);
            CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""), msg: NSLocalizedString("SocketWarn1", comment: "") + String(self.reconnectAttempts) + NSLocalizedString("SocketWarn2", comment: ""), h1Text: NSLocalizedString("Close", comment: ""), h1: nil)
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + self.socketReconnectTime) { // Try to reconnect with a timer
            if (self.socket != nil && !self.socket!.isConnected){ // Do we still have a socket? no? create a new one
                self.createSocket()
                self.reconnectAttempts += 1
                Sensor.setLostPackets(number: 1);
            }
            if User.GetLoginStatus() { // make sure the user is logged in and send a warning
                Sensor.setActive(tempActive: false);
                CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""), msg: NSLocalizedString("SocketNoCon", comment: ""), h1Text: NSLocalizedString("Close", comment: ""), h1: nil)
            }
        }
    }
    
    /// Did we receive text?
    /// - Parameters:
    ///   - socket: The socket
    ///   - text: The text
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
    }
    
    
    /// Did we receive data?
    /// - Parameters:
    ///   - socket: The socket
    ///   - data: The data
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
    }
}
