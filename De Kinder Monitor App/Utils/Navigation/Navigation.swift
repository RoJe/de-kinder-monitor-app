//
//  Navigation.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/10/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit
class Navigation {
    
    /// Replace the root for so the user can't ever get back to it
    /// - Parameters:
    ///   - strb: storyboard
    ///   - view: view inside the storyboard
    static func ReplaceRoot(strb: String, view: String){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: strb, bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: view)
        UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController = viewController
    }
    
    /// Method to replace the root but allow an custom controller
    /// - Parameters:
    ///   - view: The view ( custom controller )
    ///   - anim: should it animate?
    static func ReplaceRootWithController(view: Any, anim: Bool) {
        UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController = view as? UIViewController;
    }
    
    /// Go to new view
    /// - Parameters:
    ///   - strb: storyboard
    ///   - view: the view
    ///   - anim: should it animate?
    static func Go(strb: String, view: String, anim: Bool){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: strb, bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: view)
        let navController = Navigation.GetBottomViewController()
        navController?.show(viewController, sender: Any?.self)
        //navController?.pushViewController(viewController, animated: anim)
    }
    
    /// Get the absolute top viewcontroller and add something now to it
    /// - Parameters:
    ///   - strb: storyboard
    ///   - view: the view
    ///   - anim: should it animate?
    static func GoTop(strb: String, view: String, anim: Bool){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: strb, bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: view)
        
        Navigation.GetTopViewController()?.show(viewController, sender: Any?.self)
    }
    
    /// Method the same as GoTop but allows a custom controller
    /// - Parameters:
    ///   - view: the view
    ///   - anim: should it animatie?
    static func GoTopWithController(view: Any, anim: Bool){
        Navigation.GetTopViewController()?.show(view as! UIViewController, sender: Any?.self);
        
    }
    
    
    /// Present a view
    /// - Parameters:
    ///   - strb: storyboard
    ///   - view: the view
    ///   - anim: should it animate?
    static func Present(strb: String, view: String, anim: Bool){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: strb, bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: view)
        let navController = Navigation.GetBottomViewController()
        navController?.present(viewController, animated: anim, completion: nil)
    }
    
    /// Present with custom controlelr
    /// - Parameters:
    ///   - view: The view
    ///   - anim: Should it animate?
    static func PresentWithController(view: Any, anim: Bool){
        let navController = Navigation.GetBottomViewController()
        navController?.present(view as! UIViewController, animated: anim);
    }
    
    /// Same as go but with custom controller
    /// - Parameters:
    ///   - view: the view
    ///   - anim: Should it animate
    static func GoWithController(view: Any, anim: Bool){
        let navController = Navigation.GetBottomViewController()
        navController?.show(view as! UIViewController, sender: Any?.self);
    }
    
    /// Get the bottow layer vieuw controller
    static func GetBottomViewController() -> UINavigationController?{
        return  UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController as? UINavigationController
    }
    
    /// Get the mos t top view controller
    static func GetTopViewController() -> UINavigationController?{
        var topController: UIViewController? = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        
        return  topController as? UINavigationController
    }
    
    /// Get writ of the presented viewcontroller
    /// - Parameter anim: <#anim description#>
    static func DismissPresented(anim: Bool){
        let navController = Navigation.GetBottomViewController()
        navController?.dismiss(animated: anim, completion: nil)
    }
    
    /// Pop the view
    static func pop(){
        let navController = Navigation.GetTopViewController()
        navController!.popViewController(animated: true)
    }
    
    /// pop the view
    static func popTop(){
        let navController = Navigation.GetBottomViewController()
        navController!.popViewController(animated: true)
    }
}
