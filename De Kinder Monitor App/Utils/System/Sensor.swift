//
//  SensorData.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 1/5/20.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

/// The limits data structure for a docter
struct DocterLimits {
    var min: Int
    var max: Int
    var patientID: Int
}

/// The limits structure for a normal sensor
struct SensorLimits {
    var min: Int
    var max: Int
}

/// Possible data for a sensor
struct SensorData {
    var i: Int
    var s: String
}


class Sensor {
    
    /// Because swift is stupid, we initialize statics this way;
    /// Fallback values for the sensor
    private static var HearthBeatFallback: SensorLimits = SensorLimits(min: 80, max: 140)
    
    /// Because swift is stupid, we initialize statics this way
    /// Fallback values for the sensor
    private static var TemperatureFallback: SensorLimits = SensorLimits(min: 35, max: 38)
    
    /// Because swift is stupid, we initialize statics this way
    /// Fallback values for the sensor
    private static var FrequancyFallback: SensorLimits = SensorLimits(min: 15, max: 30)
    
    /// Because swift is stupid, we initialize statics this way
    /// Fallback values for the sensor
    private static var SaturationFallback: SensorLimits = SensorLimits(min: 85, max: 95)
    
    /// Time for when the sensor should be allowed to update
    private static var SensorUpdateInterval: Double = 1
    
    /// Global alert time
    private static var AlertTime: Double = 0
    
    /// Alert time for specific catergory
    private static var HeartAlertTime: Double = 0
    
    /// Alert time for specific catergory
    private static var TempAlertTime: Double = 0
    
    /// Alert time for specific catergory
    private static var FreqAlertTime: Double = 0
    
    /// Alert time for specific catergory
    private static var SatAlertTime: Double = 0
    
    
    /// Global next alert time
    private static var NextAlertTime: Double = 60
    
    /// Because swift is stupid, we initialize statics this way
    /// Next alert time for specific catergory
    private static var NextAlertTimeHeart: Double = (
        UserDefaults.standard.double(forKey: "HeartBeatSnooze") != 0 ?
        UserDefaults.standard.double(forKey: "HeartBeatSnooze") : NextAlertTime
    );
    
    /// Because swift is stupid, we initialize statics this way
    /// Next alert time for specific catergory
    private static var NextAlertTimeTemp: Double = (
        UserDefaults.standard.double(forKey: "TemperatureSnooze") != 0 ?
        UserDefaults.standard.double(forKey: "TemperatureSnooze") : NextAlertTime
    );
    
    /// Because swift is stupid, we initialize statics this way
    /// Next alert time for specific catergory
    private static var NextAlertTimeFreq: Double = (
        UserDefaults.standard.double(forKey: "FrequencySnooze") != 0 ?
        UserDefaults.standard.double(forKey: "FrequencySnooze") : NextAlertTime
    );
    
    /// Because swift is stupid, we initialize statics this way
    /// Next alert time for specific catergory
    private static var NextAlertTimeSat: Double = (
        UserDefaults.standard.double(forKey: "SaturationSnooze") != 0 ?
        UserDefaults.standard.double(forKey: "SaturationSnooze") : NextAlertTime
    );
    
    /// Because swift is stupid, we initialize statics this way
    /// Set the limits
    private static var HearthBeatLimit: SensorLimits = SensorLimits(
        min: (
                UserDefaults.standard.integer(forKey: "HearthBeatMin") != 0 ?
                UserDefaults.standard.integer(forKey: "HearthBeatMin") : Sensor.HearthBeatFallback.min
        ),
        max: (
                UserDefaults.standard.integer(forKey: "HearthBeatMax") != 0 ?
                UserDefaults.standard.integer(forKey: "HearthBeatMax") : Sensor.HearthBeatFallback.max
        )
    )
    
    /// Because swift is stupid, we initialize statics this way
    /// Set the limits
    private static var TemperatureLimit: SensorLimits = SensorLimits(
        min: (
                UserDefaults.standard.integer(forKey: "TemperatureMin") != 0 ?
                UserDefaults.standard.integer(forKey: "TemperatureMin") : Sensor.TemperatureFallback.min
        ),
        max: (
                UserDefaults.standard.integer(forKey: "TemperatureMax") != 0 ?
                UserDefaults.standard.integer(forKey: "TemperatureMax") : Sensor.TemperatureFallback.max
        )
    )
    
    /// Because swift is stupid, we initialize statics this way
    /// Set the limits
    private static var FrequancyLimit: SensorLimits = SensorLimits(
        min: (
                UserDefaults.standard.integer(forKey: "FrequencyMin") != 0 ?
                UserDefaults.standard.integer(forKey: "FrequencyMin") : Sensor.FrequancyFallback.min
        ),
        max: (
                UserDefaults.standard.integer(forKey: "FrequentyMax") != 0 ?
                UserDefaults.standard.integer(forKey: "FrequentyeMax") : Sensor.FrequancyFallback.max
        )
    )
    
    /// Because swift is stupid, we initialize statics this way
    /// Set the limits
    private static var SaturationLimit: SensorLimits = SensorLimits(
        min: (
                UserDefaults.standard.integer(forKey: "SaturationMin") != 0 ?
                UserDefaults.standard.integer(forKey: "SaturationMin") : Sensor.SaturationFallback.min
        ),
        max: (
                UserDefaults.standard.integer(forKey: "SaturationMax") != 0 ?
                UserDefaults.standard.integer(forKey: "SaturationMax") : Sensor.SaturationFallback.max
        )
    )
    
    
    /// Boolean to check if the sensor has bene initialized, or rather should be re-initailized
    private static var isInitialized: Bool = false
    
    /// actual sensor
    private static var HeartSensor: SensorApiData?
    
    /// actual sensor
    private static var TempSensor: SensorApiData?
    
    /// actual sensor
    private static var FreqSensor: SensorApiData?
    
    /// actual sensor
    private static var SatSensor: SensorApiData?
    
    /// Sensor data limits for the docter
    private static var DocterHearthBeatLimit: [DocterLimits]?
    
    /// Sensor data limits for the docter
    private static var DocterTemperatureLimit: [DocterLimits]?
    
    /// Sensor data limits for the docter
    private static var DocterFrequancyLimit: [DocterLimits]?
    
    /// Sensor data limits for the docter
    private static var DocterSaturationLimit: [DocterLimits]?
    
    ///sensor data for the patients form the docter
    private static var DocterHearthBeat: [SensorData]?
    
    ///sensor data for the patients form the docter
    private static var DocterTemperature: [SensorData]?
    
    ///sensor data for the patients form the docter
    private static var DocterFrequancy: [SensorData]?
    
    ///sensor data for the patients form the docter
    private static var DocterSaturation: [SensorData]?
    
    /// Actual sensor data
    private static var HearthBeat:  SensorData = SensorData(i:0, s: "0")
    
    /// Actual sensor data
    private static var Temperature: SensorData = SensorData(i:0, s: "0")
    
    /// Actual sensor data
    private static var Frequancy:   SensorData = SensorData(i:0, s: "0")
    
    /// Actual sensor data
    private static var Saturation:  SensorData = SensorData(i:0, s: "0")
    
    /// See if the sensor is active
    private static var Active: Bool = false;
    
    /// the amount of lost packets
    private static var LostPackets: Int = 0;
    
    /// Time when we connected
    private static var ConnectTime: Date = Date.init();
    
    /// Allowed delay in ethernet speed
    private static var allowdConnectionDelaySeconds: Int = 1
    
    /// Max problems allowed before warning
    private static var allowdConnectionProblems: Int = 2
    
    /// Current problems
    private static var currentConnectionProblems: Int = 0
    
    
    /// Initialize all the possible sensor values
    private static func Initialize(){
        GetSensorLimits()
    }
    
    /// destoy variables
    public static func destroy(){
        Sensor.isInitialized = false
        Sensor.HeartSensor = nil
        Sensor.SatSensor = nil
        Sensor.TempSensor = nil
        Sensor.FreqSensor = nil
        
        Sensor.DocterHearthBeat = nil
        Sensor.DocterFrequancy = nil
        Sensor.DocterSaturation = nil
        Sensor.DocterTemperature = nil
    }
    
    
    /// Get sensor limits from APi
    private static func GetSensorLimits(){
        Network.GetConnectedPatients(urlFail: {
            
        }, comp: { (json, error, response) in
            if response.response?.statusCode == 200 {
                if let json = json {
                    do {
                        let patients = try JSONDecoder().decode([PatientData].self, from: Data(json.utf8) )
                        Network.GetSensors(
                            patientID: patients.first?.patientID ?? 0
                            , urlFail: {
                                
                            }, comp: { (json, error, response) in
                                if response.response?.statusCode == 200 {
                                    if let json = json {
                                        do{
                                            let sensors = try JSONDecoder().decode([SensorApiData].self, from: Data(json.utf8) )
                                            for sensor in sensors  {
                                                if sensor.type == "Heart" {
                                                    Sensor.HeartSensor = sensor
                                                    
                                                    let min = (sensor.thresholdMin != 0 ? sensor.thresholdMin : Sensor.HearthBeatFallback.min)
                                                    let max = (sensor.thresholdMax != 0 ? sensor.thresholdMax : Sensor.HearthBeatFallback.max)
                                                    
                                                    Sensor.HearthBeatLimit = SensorLimits(
                                                        min: min,
                                                        max: max
                                                    )
                                                }
                                                if sensor.type == "Temp" {
                                                    Sensor.TempSensor = sensor
                                                    
                                                    let min = ( sensor.thresholdMin != 0 ? sensor.thresholdMin : Sensor.TemperatureFallback.min)
                                                    let max = (sensor.thresholdMax != 0 ? sensor.thresholdMax : Sensor.TemperatureFallback.max)
                                                    
                                                    Sensor.TemperatureLimit = SensorLimits(
                                                        min: min,
                                                        max: max
                                                    )
                                                }
                                                if sensor.type == "Freq" {
                                                    Sensor.FreqSensor = sensor
                                                    
                                                    let min = ( sensor.thresholdMin != 0 ? sensor.thresholdMin : Sensor.FrequancyFallback.min)
                                                    let max = (sensor.thresholdMax != 0 ? sensor.thresholdMax : Sensor.FrequancyFallback.max)
                                                    
                                                    Sensor.FrequancyLimit = SensorLimits(
                                                        min: min,
                                                        max: max
                                                    )
                                                }
                                                if sensor.type == "Sat" {
                                                    Sensor.SatSensor = sensor
                                                    
                                                    let min = ( sensor.thresholdMin != 0 ? sensor.thresholdMin : Sensor.SaturationFallback.min)
                                                    let max = (sensor.thresholdMax != 0 ? sensor.thresholdMax : Sensor.SaturationFallback.max)
                                                    
                                                    Sensor.SaturationLimit = SensorLimits(
                                                        min: min,
                                                        max: max
                                                    )
                                                    
                                                }
                                            }
                                        }catch{
                                        }
                                    }
                                }
                            },fail: { (error) in
                        })
                    }catch {
                    }
                    
                }
                
            }
            
            }, fail: { (error) in
        })
        
        Sensor.isInitialized = true
    }
    
    /// Initialize the docter monitor values
    /// - Parameter id: is of the patient
    private static func DocterInitialize(id: Int){
        let defaults = UserDefaults.standard
        let heartMinKey = "HearthBeatMin" + String(id);
        let heartMaxKey = "HearthBeatMax" + String(id);
        let tempMinKey = "TemperatureMin" + String(id);
        let tempMaxKey = "TemperatureMax" + String(id);
        let freqMinKey = "FrequencyMin" + String(id);
        let freqMaxKey = "FrequencyMax" + String(id);
        let satMinKey = "SaturationMin" + String(id);
        let satMaxKey = "SaturationMax" + String(id);
        
        if (Sensor.DocterHearthBeatLimit == nil) {
            Sensor.DocterHearthBeatLimit = [DocterLimits(
                min: ((defaults.integer(forKey: heartMinKey) != 0 ) ? defaults.integer(forKey: heartMinKey) : Sensor.HearthBeatFallback.min),
                max: ((defaults.integer(forKey: heartMaxKey) != 0 ) ? defaults.integer(forKey: heartMaxKey) : Sensor.HearthBeatFallback.max), patientID: id
            )]
        } else {
            Sensor.DocterHearthBeatLimit?.append(DocterLimits(
                min: ((defaults.integer(forKey: heartMinKey) != 0 ) ? defaults.integer(forKey: heartMinKey) : Sensor.HearthBeatFallback.min),
                max: ((defaults.integer(forKey: heartMaxKey) != 0 ) ? defaults.integer(forKey: heartMaxKey) : Sensor.HearthBeatFallback.max), patientID: id
            ))
        }
        
        if(Sensor.DocterTemperatureLimit == nil) {
            Sensor.DocterTemperatureLimit = [DocterLimits(
                min: ((defaults.integer(forKey: tempMinKey) != 0 ) ? defaults.integer(forKey: tempMinKey) : Sensor.TemperatureFallback.min),
                max: ((defaults.integer(forKey: tempMaxKey) != 0 ) ? defaults.integer(forKey: tempMaxKey) : Sensor.TemperatureFallback.max), patientID: id
            )]
        } else {
            Sensor.DocterTemperatureLimit?.append(DocterLimits(
                min: ((defaults.integer(forKey: tempMinKey) != 0 ) ? defaults.integer(forKey: tempMinKey) : Sensor.TemperatureFallback.min),
                max: ((defaults.integer(forKey: tempMaxKey) != 0 ) ? defaults.integer(forKey: tempMaxKey) : Sensor.TemperatureFallback.max), patientID: id
            ))
        }
        
        if(Sensor.DocterFrequancyLimit == nil) {
            Sensor.DocterFrequancyLimit = [DocterLimits(
                min: ((defaults.integer(forKey: freqMinKey) != 0 ) ? defaults.integer(forKey: freqMinKey) : Sensor.FrequancyFallback.min),
                max: ((defaults.integer(forKey: freqMaxKey) != 0 ) ? defaults.integer(forKey: freqMaxKey) : Sensor.FrequancyFallback.max), patientID: id
            )]
        } else {
            Sensor.DocterFrequancyLimit?.append(DocterLimits(
                min: ((defaults.integer(forKey: freqMinKey) != 0 ) ? defaults.integer(forKey: freqMinKey) : Sensor.FrequancyFallback.min),
                max: ((defaults.integer(forKey: freqMaxKey) != 0 ) ? defaults.integer(forKey: freqMaxKey) : Sensor.FrequancyFallback.max), patientID: id
            ))
        }
        
        if(Sensor.DocterSaturationLimit == nil) {
            Sensor.DocterSaturationLimit = [DocterLimits(
                min: ((defaults.integer(forKey: satMinKey) != 0 ) ? defaults.integer(forKey: satMinKey) : Sensor.SaturationFallback.min),
                max: ((defaults.integer(forKey: satMaxKey) != 0 ) ? defaults.integer(forKey: satMaxKey) : Sensor.SaturationFallback.max), patientID: id
            )];
        } else {
            Sensor.DocterSaturationLimit?.append(DocterLimits(
                min: ((defaults.integer(forKey: satMinKey) != 0 ) ? defaults.integer(forKey: satMinKey) : Sensor.SaturationFallback.min),
                max: ((defaults.integer(forKey: satMaxKey) != 0 ) ? defaults.integer(forKey: satMaxKey) : Sensor.SaturationFallback.max), patientID: id
            ));
        }

        NextAlertTimeHeart = Double(((defaults.integer(forKey: "HeartBeatSnooze") != 0 ) ? Double(defaults.integer(forKey: "HeartBeatSnooze")) : NextAlertTime));
        NextAlertTimeTemp = Double(((defaults.integer(forKey: "TemperatureSnooze") != 0 ) ? Double(defaults.integer(forKey: "TemperatureSnooze")) : NextAlertTime))
        NextAlertTimeFreq = Double(((defaults.integer(forKey: "FrequencySnooze") != 0 ) ? Double(defaults.integer(forKey: "FrequencySnooze")) : NextAlertTime))
        NextAlertTimeSat = Double(((defaults.integer(forKey: "SaturationSnooze") != 0 ) ? Double(defaults.integer(forKey: "SaturationSnooze")) : NextAlertTime))
        Active = true;
    }
    
    
    
    /// When can we alert?
    static func GetSensorUpdateInterval() -> Double {
        return Sensor.SensorUpdateInterval
    }
    
    
    /// Can we actually alert?
    static func CanAlert() -> Bool {
        if Sensor.AlertTime < CACurrentMediaTime() {
            Sensor.AlertTime = CACurrentMediaTime() + Sensor.NextAlertTime
            return true
        }
        return false
    }
    
    /// Can we alert heart?
    static func HeartCanAlert() -> Bool {
        if Sensor.HeartAlertTime < CACurrentMediaTime() {
            Sensor.HeartAlertTime = CACurrentMediaTime() + Sensor.NextAlertTimeHeart
            return true
        }
        return false
    }
    
    /// Can we alert temperature?
    static func TempCanAlert() -> Bool {
        if Sensor.TempAlertTime < CACurrentMediaTime() {
            Sensor.TempAlertTime = CACurrentMediaTime() + Sensor.NextAlertTimeTemp
            return true
        }
        return false
    }
    
    /// can we alert prequancy?
    static func FreqCanAlert() -> Bool {
        if Sensor.FreqAlertTime < CACurrentMediaTime() {
            Sensor.FreqAlertTime = CACurrentMediaTime() + Sensor.NextAlertTimeFreq
            return true
        }
        return false
    }
    
    static func SatCanAlert() -> Bool {
        if Sensor.SatAlertTime < CACurrentMediaTime() {
            Sensor.SatAlertTime = CACurrentMediaTime() + Sensor.NextAlertTimeSat
            return true
        }
        return false
    }
    
    /// Function to create an alert
    /// - Parameters:
    ///   - warningMsg: wanring message
    ///   - viewController: viewcontroller to redirect to
    static func SensorAlert(warningMsg: String, viewController: String) {
        CustomAlert.Show(
            title: NSLocalizedString("Warning", comment: ""),
            msg: warningMsg,
            h1Text: NSLocalizedString("MoreInfo", comment: ""),
            h2Text: NSLocalizedString("EmergencyNumber", comment: ""),
            h3Text: NSLocalizedString("Snooze", comment: ""),
        h1: { uialert in
            Navigation.DismissPresented(anim: true)
            Navigation.Present(strb: "Details", view: viewController, anim: true)
        }, h2: { uialert in
            let url: NSURL = URL(string: "tel:112")! as NSURL
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }, h3: nil)
    }
    
    /// check if the values are initialized
    private static func IsInitialized() -> Bool{
        return Sensor.isInitialized
    }
    
    /// Is the docter initialized
    /// - Parameter id: id of the patient
    private static func DocterIsInitialized(id: Int) -> Bool{
        if (Sensor.DocterTemperatureLimit == nil || Sensor.DocterFrequancyLimit == nil) {
            return false;
        }
        
        var initialized = false;
        for item in Sensor.DocterTemperatureLimit! {
            if (item.patientID == id) {
                initialized = true;
                break;
            }
        }
        if(!initialized) {
            return false
        }
        for item in Sensor.DocterFrequancyLimit! {
            if (item.patientID == id) {
                initialized = true;
                break;
            }
        }
        if(!initialized) {
            return false
        }
        for item in Sensor.DocterHearthBeatLimit! {
            if (item.patientID == id) {
                initialized = true;
                break;
            }
        }
        if(!initialized) {
            return false
        }
        for item in Sensor.DocterSaturationLimit! {
            if (item.patientID == id) {
                initialized = true;
                break;
            }
        }
        if(!initialized) {
            return false
        }
        return false;
    }
    
    /// Set the snooze time for the heart
    /// - Parameter timeToAdd: <#timeToAdd description#>
    static func SetHeartBeatSnooze(timeToAdd: Double) {
        NextAlertTimeHeart = timeToAdd;
        let defaults = UserDefaults.standard
        defaults.setValue(timeToAdd, forKey: "HeartBeatSnooze");
        defaults.synchronize()
    }
    
    /// Get heart snooze time
    static func GetHeartBeatSnooze() -> Double {
        return NextAlertTimeHeart;
    }
    
    /// Set the hearthbeat limits
    /// - Parameters:
    ///   - min: minimum limit
    ///   - max: maximum limit
    static func SetHeartBeatLimits(min: Int, max: Int) {
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        let defaults = UserDefaults.standard
        defaults.setValue(min, forKey: "HearthBeatMin");
        defaults.setValue(max, forKey: "HearthBeatMax");
        defaults.synchronize()
        Sensor.HearthBeatLimit = SensorLimits(min: min, max: max)
        
        if Sensor.HeartSensor == nil {
            return;
        }
        
        Sensor.HeartSensor?.thresholdMin = min
        Sensor.HeartSensor?.thresholdMax = max
        Network.UpdateSensorData(
            sensor: Sensor.HeartSensor!,
            urlFail: {
            
            }, comp: { (json, error, response) in
                
            }, fail: { (error) in
        })
    }
    
    /// Set heart limits for the docter
    /// - Parameters:
    ///   - min: min value
    ///   - max: max value
    ///   - id: patient id
    static func SetDocterHeartBeatLimits(min: Int, max: Int, id: Int) {
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id);
        }
        let stringId = String(id);
        let minKey = "HearthBeatMin" + stringId;
        let maxKey = "HearthBeatMax" + stringId;
        let defaults = UserDefaults.standard
        defaults.setValue(min, forKey: minKey);
        defaults.setValue(max, forKey: maxKey);
        defaults.synchronize()
        var i = 0;
        for item in Sensor.DocterHearthBeatLimit! {
            if (item.patientID == id) {
                Sensor.DocterHearthBeatLimit?[i].min = min;
                Sensor.DocterHearthBeatLimit?[i].max = max;
                return
            }
            i += 1;
        }
    }
    
    /// Get the heart fallback values
    static func getHeartBeatFallback() -> SensorLimits {
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        
        return HearthBeatFallback;
    }
    
    /// Get the hearthbeat limits
    static func GetHeartBeatLimits() -> SensorLimits {
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        
        return Sensor.HearthBeatLimit
    }
    
    /// Get heart limits
    /// - Parameter id: patient id
    static func GetDocterHeartBeatLimits(id: Int) -> DocterLimits? {
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id)
        }
        for item in Sensor.DocterHearthBeatLimit! {
            if (item.patientID == id) {
                return (item);
            }
        }
        return nil
    }
    
    /// Set the hearthbeat with an string
    /// - Parameter val: hearthbeat value in string
    static func SetHearthBeat(val: String){
        Sensor.HearthBeat = SensorData (
                i: Int(val) ?? 0,
                s: val
            )
    }
    
    // Set the hearthbeat with an int
    /// - Parameter val: hearthbeat value
    static func SetHearthBeat(val: Int){
        Sensor.HearthBeat = SensorData (
            i: val,
            s: String(val)
        )
    }
    
    /// Set the actual heartbeat value
    /// - Parameter values: value
    static func SetDocterHeartBeat(values: [SensorData]) {
        Sensor.DocterHearthBeat = values;
    }
    
    /// Get the hearthbeat value
    static func GetHearthBeat() -> Int{
        return Sensor.HearthBeat.i
    }
    
    /// Get the hearthbeat value
    static func GetHeartBeat() -> String{
        return Sensor.HearthBeat.s
    }
    
    /// Get the hearthbeat value
    static func GetHearthBeat() -> SensorData{
        return Sensor.HearthBeat
    }
    
    /// Get heartbeat
    /// - Parameter rowIndex: the value
    static func GetDocterHeartBeat(rowIndex: Int) -> String {
        if(Sensor.DocterHearthBeat?[rowIndex] != nil) {
            return (Sensor.DocterHearthBeat?[rowIndex].s)!;
        } else {
            return "-1";
        }
    }
    
    /// Check if the given number is within bounds
    /// - Parameters:
    ///   - val: value
    static func IsWithinHeartBeatBounds() -> Bool{
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        return (Sensor.GetHearthBeat() > Sensor.GetHeartBeatLimits().min && Sensor.GetHearthBeat() < Sensor.GetHeartBeatLimits().max)
    }
    
    /// Is the heart within the allowed limits?
    /// - Parameters:
    ///   - id: patient id
    ///   - rowIndex: index for the value
    static func IsWithinDocterHeartBeatBounds(id: Int, rowIndex: Int) -> Bool{
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id)
        }
        
        let limits = Sensor.GetDocterHeartBeatLimits(id: id)
        let value = Sensor.GetDocterHeartBeat(rowIndex: rowIndex);
        
        let returnVal1 = Int(value)! > limits!.min
        let returnVal2 = Int(value)! < limits!.max
        return (returnVal1 && returnVal2)
    }
    
    /// Set the snooze time for tempertaure
    /// - Parameter timeToAdd: time
    static func SetTemperatureSnooze(timeToAdd: Double) {
        NextAlertTimeTemp = timeToAdd;
        let defaults = UserDefaults.standard
        defaults.setValue(timeToAdd, forKey: "TemperatureSnooze");
        defaults.synchronize()
    }
    
    /// Set the temperature limits
    /// - Parameters:
    ///   - min: min value
    ///   - max: max value
    static func SetTemperatureLimits(min: Int, max: Int) {
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        let defaults = UserDefaults.standard
        defaults.setValue(min, forKey: "TemperatureMin");
        defaults.setValue(max, forKey: "TemperatureMax");
        defaults.synchronize()
        Sensor.TemperatureLimit = SensorLimits(min: min, max: max)
        
        if Sensor.TempSensor == nil {
            return;
        }
        
        
        Sensor.TempSensor?.thresholdMin = min
        Sensor.TempSensor?.thresholdMax = max
        Network.UpdateSensorData(
            sensor: Sensor.TempSensor!,
            urlFail: {
            
            }, comp: { (json, error, response) in
                
            }, fail: { (error) in
        })
    }
    
    
    /// Set the docter temperature limits
    /// - Parameters:
    ///   - min: min value
    ///   - max: max value
    ///   - id: patient id
    static func SetDocterTempLimits(min: Int, max: Int, id: Int) {
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id);
        }
        let stringId = String(id);
        let minKey = "TemperatureMin" + stringId;
        let maxKey = "TemperatureMax" + stringId;
        let defaults = UserDefaults.standard
        defaults.setValue(min, forKey: minKey);
        defaults.setValue(max, forKey: maxKey);
        defaults.synchronize()
        var i = 0;
        for item in Sensor.DocterTemperatureLimit! {
            if (item.patientID == id) {
                Sensor.DocterTemperatureLimit?[i].min = min;
                Sensor.DocterTemperatureLimit?[i].max = max;
                return
            }
            i += 1;
        }
    }
    
    
    /// Get the temperature snooze time
    static func GetTemperatureSnooze() -> Double {
        return NextAlertTimeTemp;
    }
    
    
    /// Get the temperature fallback values
    static func getTemperatureFallback() -> SensorLimits {
        return TemperatureFallback;
    }
    
    /// Get the temperature limits
    static func GetTemperatureLimits() -> SensorLimits {
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        
        return Sensor.TemperatureLimit
    }
    
    /// Get the docter termperature fallback values
    /// - Parameter id: patient id
    static func GetDocterTemperatureLimits(id: Int) -> DocterLimits? {
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id)
        }
        for item in Sensor.DocterTemperatureLimit! {
            if (item.patientID == id) {
                return (item);
            }
        }
        return nil
    }
    
    /// Set the Temperature with an string
    /// - Parameter val: hearthbeat value in string
    static func SetTemperature(val: String){
        Sensor.Temperature = SensorData(
                i: Int(val) ?? 0,
                s: val
            )
    }
    
    // Set the Temperature with an int
    /// - Parameter val: hearthbeat value
    static func SetTemperature(val: Int){
        Sensor.Temperature = SensorData(
            i: val,
            s: String(val)
        )
    }
    
    /// Se the temperature for a patient
    /// - Parameter values: value
    static func SetDocterTemperature(values: [SensorData]) {
        Sensor.DocterTemperature = values;
    }
    
    /// Get the Temperature value
    static func GetTemperature() -> Int{
        return Sensor.Temperature.i
    }
    
    /// Get the Temperature value
    static func GetTemperature() -> String{
        return Sensor.Temperature.s;
    }
    
    /// Get the temperature value
    /// - Parameter rowIndex: index for value
    static func GetDocterTemperature(rowIndex: Int) -> String {
        if(Sensor.DocterTemperature?[rowIndex] != nil) {
            return (Sensor.DocterTemperature?[rowIndex].s)!;
        } else {
            return "-1";
        }
    }
    
    /// Get the Temperature value
    static func GetTemperature() -> SensorData{
        return Sensor.Temperature
    }
    
    /// Check if the given number is within bounds
    /// - Parameters:
    ///   - val: value
    static func IsWithinTemperatureBounds() -> Bool{
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        return (Sensor.GetTemperature() > Sensor.TemperatureLimit.min && Sensor.GetTemperature() < Sensor.TemperatureLimit.max)
    }
    
    /// Is the temperature within the given limits
    /// - Parameters:
    ///   - id: patient id
    ///   - rowIndex: index for value
    static func IsWithinDocterTemperatureBounds(id: Int, rowIndex: Int) -> Bool{
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id)
        }
        
        let limits = Sensor.GetDocterTemperatureLimits(id: id)
        let value = Sensor.GetDocterTemperature(rowIndex: rowIndex);
        
        let returnVal1 = Int(value)! > limits!.min
        let returnVal2 = Int(value)! < limits!.max
        return (returnVal1 && returnVal2)
    }
    
    
    /// Set the snooze time for the frequancy alert
    /// - Parameter timeToAdd: to for the snooze
    static func SetFrequencySnooze(timeToAdd: Double) {
        NextAlertTimeFreq = timeToAdd;
        let defaults = UserDefaults.standard
        defaults.setValue(timeToAdd, forKey: "frequencySnooze");
        defaults.synchronize()
    }
    
    /// Set the frequancy limits
    /// - Parameters:
    ///   - min: min value
    ///   - max: max value
    static func SetFrequancyLimits(min: Int, max: Int) {
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        let defaults = UserDefaults.standard
        defaults.setValue(min, forKey: "FrequencyMin");
        defaults.setValue(max, forKey: "FrequencyMax");
        defaults.synchronize()
        Sensor.FrequancyLimit = SensorLimits(min: min, max: max)
        
        
        if Sensor.FreqSensor == nil {
            return;
        }
        
        
        Sensor.FreqSensor?.thresholdMin = min
        Sensor.FreqSensor?.thresholdMax = max
        Network.UpdateSensorData(
            sensor: Sensor.FreqSensor!,
            urlFail: {
            
            }, comp: { (json, error, response) in
                
            }, fail: { (error) in
        })
    }
    
    /// Set the frequancy limits
    /// - Parameters:
    ///   - min: min value
    ///   - max: max value
    ///   - id: patient id
    static func SetDocterFrequencyLimits(min: Int, max: Int, id: Int) {
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id);
        }
        let stringId = String(id);
        let minKey = "FrequencyMin" + stringId;
        let maxKey = "FrequencyMax" + stringId;
        let defaults = UserDefaults.standard
        defaults.setValue(min, forKey: minKey);
        defaults.setValue(max, forKey: maxKey);
        defaults.synchronize()
        var i = 0;
        for item in Sensor.DocterFrequancyLimit! {
            if (item.patientID == id) {
                Sensor.DocterFrequancyLimit?[i].min = min;
                Sensor.DocterFrequancyLimit?[i].max = max;
                return
            }
            i += 1;
        }
    }
    
    /// Get the frequancy fallback values
    static func getFrequancyFallback() -> SensorLimits {
        return FrequancyFallback;
    }
    
    /// Get the frequancy limits
    static func GetFrequancyLimits() -> SensorLimits {
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        
        return Sensor.FrequancyLimit
    }
    
    /// Get the docter frequancy fallback values
    /// - Parameter id: patient id
    static func GetDocterFrequencyLimits(id: Int) -> DocterLimits? {
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id)
        }
        for item in Sensor.DocterFrequancyLimit! {
            if (item.patientID == id) {
                return (item);
            }
        }
        return nil
    }
    
    /// Get the frequancy snooze time
    static func GetFrequencySnooze() -> Double {
        return NextAlertTimeFreq;
    }
    
    /// Set the Frequancy with an string
    /// - Parameter val: hearthbeat value in string
    static func SetFrequancy(val: String){
        Sensor.Frequancy = SensorData(
            i: Int(val) ?? 0,
            s: val
        )
    }
    
    // Set the Frequancy with an int
    /// - Parameter val: hearthbeat value
    static func SetFrequancy(val: Int){
        Sensor.Frequancy = SensorData(
            i: val,
            s: String(val)
        )
    }
    
    /// Get the Frequancy value
    static func GetFrequancy() -> Int{
            return Sensor.Frequancy.i
    }
    
    /// Set the docter frequancy value
    /// - Parameter values: value
    static func SetDocterFrequency(values: [SensorData]) {
        Sensor.DocterFrequancy = values;
    }
    
    /// Get the Frequancy value
    static func GetFrequancy() -> String{
        return Sensor.Frequancy.s
    }
    
    /// Get the Frequancy value
    static func GetFrequancy() -> SensorData{
        return Sensor.Frequancy
    }
    
    
    /// Get the docter frequancy value
    /// - Parameter rowIndex: index for value
    static func GetDocterFrequency(rowIndex: Int) -> String {
        if(Sensor.DocterFrequancy?[rowIndex] != nil) {
            return (Sensor.DocterFrequancy?[rowIndex].s)!;
        } else {
            return "-1";
        }
    }
    
    /// Check if the given number is within bounds
    /// - Parameters:
    ///   - val: value
    static func IsWithinFrequancyBounds() -> Bool{
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        
        return (Sensor.GetFrequancy() > Sensor.FrequancyLimit.min && Sensor.GetFrequancy() < Sensor.FrequancyLimit.max)
    }
    
    /// Is the frequancy within given limits
    /// - Parameters:
    ///   - id: patients id
    ///   - rowIndex: index for value
    static func IsWithinDocterFrequencyBounds(id: Int, rowIndex: Int) -> Bool{
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id)
        }
        
        let limits = Sensor.GetDocterFrequencyLimits(id: id)
        let value = Sensor.GetDocterFrequency(rowIndex: rowIndex);
        
        let returnVal1 = Int(value)! > limits!.min
        let returnVal2 = Int(value)! < limits!.max
        return (returnVal1 && returnVal2)
    }
    
    /// Get the snooze time for saturation
    /// - Parameter timeToAdd: time to add to the snooze
    static func SetSaturationSnooze(timeToAdd: Double) {
        NextAlertTimeSat = timeToAdd;
        let defaults = UserDefaults.standard
        defaults.setValue(timeToAdd, forKey: "SaturationSnooze");
        defaults.synchronize()
    }
    
    /// Set the saturation limits
    /// - Parameters:
    ///   - min: min value
    ///   - max: max value
    static func SetSaturationLimits(min: Int, max: Int) {
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        let defaults = UserDefaults.standard
        defaults.setValue(min, forKey: "SaturationMin");
        defaults.setValue(max, forKey: "SaturationMax");
        defaults.synchronize()
        Sensor.SaturationLimit = SensorLimits(min: min, max: max)
        
        
        if Sensor.SatSensor == nil {
            return;
        }
        
        Sensor.SatSensor?.thresholdMin = min
        Sensor.SatSensor?.thresholdMax = max
        Network.UpdateSensorData(
            sensor: Sensor.SatSensor!,
            urlFail: {
            
            }, comp: { (json, error, response) in
                
            }, fail: { (error) in
        })
    }
    
    /// Set the saturation limits for teh odcter
    /// - Parameters:
    ///   - min: min value
    ///   - max: max value
    ///   - id: patient id
    static func SetDocterSaturationLimits(min: Int, max: Int, id: Int) {
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id);
        }
        let stringId = String(id);
        let minKey = "SaturationMin" + stringId;
        let maxKey = "SaturationMax" + stringId;
        let defaults = UserDefaults.standard
        defaults.setValue(min, forKey: minKey);
        defaults.setValue(max, forKey: maxKey);
        defaults.synchronize()
        var i = 0;
        for item in Sensor.DocterSaturationLimit! {
            if (item.patientID == id) {
                Sensor.DocterSaturationLimit?[i].min = min;
                Sensor.DocterSaturationLimit?[i].max = max;
                return
            }
            i += 1;
        }
    }
    
    /// Get the saturation snooze time
    static func GetSaturationSnooze() -> Double {
        return NextAlertTimeSat;
    }
    
    /// Get the saturation fallback values
    static func getSaturationFallback() -> SensorLimits {
        return SaturationFallback;
    }
    
    /// Get the saturation  limits
    static func GetSaturationLimits() -> SensorLimits {
        if (!Sensor.IsInitialized()){
            Sensor.Initialize()
        }
        
        return Sensor.SaturationLimit
    }
    
    /// Get the docter saturation limits
    /// - Parameter id: patient id
    static func GetDocterSaturationLimits(id: Int) -> DocterLimits? {
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id)
        }
        for item in Sensor.DocterSaturationLimit! {
            if (item.patientID == id) {
                return (item);
            }
        }
        return nil
    }
    
    /// Set the Saturation with an string
    /// - Parameter val: hearthbeat value in string
    static func SetSaturation(val: String){
        Sensor.Saturation = SensorData(
            i: Int(val) ?? 0,
            s: val
        )
    }
    
    // Set the Saturation with an int
    /// - Parameter val: hearthbeat value
    static func SetSaturation(val: Int){
        Sensor.Saturation = SensorData(
            i: val,
            s: String(val)
        )
    }
    
    /// Set the saturation for the docter
    /// - Parameter values: sat value
    static func SetDocterSaturation(values: [SensorData]) {
        Sensor.DocterSaturation = values;
    }
    
    /// Get the Saturation value
    static func GetSaturation() -> Int{
        return Sensor.Saturation.i
    }
    
    /// Get the Saturation value
    static func GetSaturation() -> String{
        return Sensor.Saturation.s
    }
    
    /// Get the Saturation value
    static func GetSaturation() -> SensorData{
        return Sensor.Saturation
    }
    
    /// Get the docter saturation
    /// - Parameter rowIndex: index for value
    static func GetDocterSaturation(rowIndex: Int) -> String {
        if(Sensor.DocterSaturation?[rowIndex] != nil) {
            return (Sensor.DocterSaturation?[rowIndex].s)!;
        } else {
            return "-1";
        }
    }
    
    /// Check if the given number is within bounds
    /// - Parameters:
    ///   - val: value
    static func IsWithinSaturationBounds() -> Bool{
       if (!Sensor.IsInitialized()){
           Sensor.Initialize()
       }
       
       return (Sensor.GetSaturation() > Sensor.SaturationLimit.min && Sensor.GetSaturation() < Sensor.SaturationLimit.max)
    }
    
    /// Is the saturation within the given limits?
    /// - Parameters:
    ///   - id: patient id
    ///   - rowIndex: index for value
    static func IsWithinDocterSaturationBounds(id: Int, rowIndex: Int) -> Bool{
        if (!Sensor.DocterIsInitialized(id: id)){
            Sensor.DocterInitialize(id: id)
        }
        
        let limits = Sensor.GetDocterSaturationLimits(id: id)
        let value = Sensor.GetDocterSaturation(rowIndex: rowIndex);
        
        let returnVal1 = Int(value)! > limits!.min
        let returnVal2 = Int(value)! < limits!.max
        return (returnVal1 && returnVal2)
    }
    
    /// Update the sensordata from an string array
    /// - Parameter values: sensor values that must be an array with the size of 4
    static func SetSensorData(values: SensorSocketData){
        let sendData = values.TimeSend.split(separator: ":")
        let receivedData = GetMinutesAndSeconds().split(separator: ":")
        
        let minutesSend = Int(sendData[0]) ?? 0
        let minutesReceived = Int(receivedData[0]) ?? 0
        
        let secondsSend = Int(sendData[1]) ?? 1
        let secondsReceived = Int(receivedData[1]) ?? 1
        
        var clear = true
        
        
        if minutesSend == minutesReceived || minutesSend == minutesReceived+1 || minutesSend == minutesReceived-1 {
            
        }else{
            clear = false
        }
        
        if secondsSend == secondsReceived || secondsSend == secondsReceived+Sensor.allowdConnectionDelaySeconds || secondsSend == Sensor.allowdConnectionDelaySeconds {
            
        }else{
            clear = false
        }
        
        if (!clear){
            Sensor.currentConnectionProblems += 1
            if Sensor.currentConnectionProblems > Sensor.allowdConnectionProblems {
                CustomAlert.Show(
                    title: NSLocalizedString("Warning", comment: ""),
                    msg: NSLocalizedString("WarnSlow", comment: ""),
                    h1Text: NSLocalizedString("Close", comment: ""),
                h1: nil)
            }
            
        }else{
            Sensor.currentConnectionProblems = 0
        }
        
        Sensor.SetHearthBeat(val: values.Heart)
        Sensor.SetTemperature(val: values.Temp)
        Sensor.SetFrequancy(val: values.Freq)
        Sensor.SetSaturation(val: values.Sat)
    }
    
    /// Set the sensordata for teh docter
    /// - Parameter values: values to insert
    static func SetDocterSensorData(values: [SensorSocketData]){
        if(values.count <= 0) {
            return;
        }
        let sendData = values[0].TimeSend.split(separator: ":")
        let receivedData = GetMinutesAndSeconds().split(separator: ":")
        
        let minutesSend = Int(sendData[0]) ?? 0
        let minutesReceived = Int(receivedData[0]) ?? 0
        
        let secondsSend = Int(sendData[1]) ?? 1
        let secondsReceived = Int(receivedData[1]) ?? 1
        
        var clear = true
        
        
        if minutesSend == minutesReceived || minutesSend == minutesReceived+1 || minutesSend == minutesReceived-1 {
            
        }else{
            clear = false
        }
        
        if secondsSend == secondsReceived || secondsSend == secondsReceived+Sensor.allowdConnectionDelaySeconds || secondsSend == Sensor.allowdConnectionDelaySeconds {
            
        }else{
            clear = false
        }
        
        
        
        if (!clear){
            Sensor.currentConnectionProblems += 1
            if Sensor.currentConnectionProblems > Sensor.allowdConnectionProblems {
                CustomAlert.Show(
                    title: NSLocalizedString("Warning", comment: ""),
                    msg: NSLocalizedString("WarnSlow", comment: ""),
                    h1Text: NSLocalizedString("Close", comment: ""),
                h1: nil)
            }
        }else{
            Sensor.currentConnectionProblems = 0
        }
        
        var TempSensorData: [SensorData]?;
        var FreqSensorData: [SensorData]?;
        var SatSensorData: [SensorData]?;
        var HeartSensorData: [SensorData]?;
        for item in values {
            let fillTempItem = SensorData(i: Int(item.Temp)!, s: item.Temp)
            let fillFreqItem = SensorData(i: Int(item.Freq)!, s: item.Freq)
            let fillSatItem = SensorData(i: Int(item.Sat)!, s: item.Sat)
            let fillHeartItem = SensorData(i: Int(item.Heart)!, s: item.Heart)
            if( TempSensorData == nil) {
                TempSensorData = [fillTempItem];
            } else {
                TempSensorData?.append(fillTempItem)
            }
            
            if( FreqSensorData == nil) {
                FreqSensorData = [fillFreqItem];
            } else {
                FreqSensorData?.append(fillFreqItem)
            }
            
            if( HeartSensorData == nil) {
                HeartSensorData = [fillHeartItem];
            } else {
                HeartSensorData?.append(fillHeartItem)
            }
            
            if( SatSensorData == nil) {
                SatSensorData = [fillSatItem];
            } else {
                SatSensorData?.append(fillSatItem)
            }
            

        }
        

        
        Sensor.SetDocterHeartBeat(values: HeartSensorData!);
        Sensor.SetDocterTemperature(values: TempSensorData!);
        Sensor.SetDocterFrequency(values: FreqSensorData!);
        Sensor.SetDocterSaturation(values: SatSensorData!);
    }
    
    /// Helper function to get curr minutes and seconds
    static private func GetMinutesAndSeconds() -> String{
        let calendar = Calendar.current
        let date = Date()
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        
        return "\(minute):\(second)"
    }
    
    ///Sensor connection info
    static func getActive() -> Bool {
        return Active;
    }
    
    /// Set if active
    /// - Parameter tempActive: activation val
    static func setActive(tempActive: Bool) {
        Active = tempActive;
    }
    
    /// Get packet loss value
    static func getLostPackets() -> Int{
        return LostPackets;
    }
    
    /// Add to packet loss value
    /// - Parameter number: number to add
    static func setLostPackets(number: Int) {
        LostPackets += number;
    }
    
    /// Get the current connection time
    static func getConnectTime() -> Date{
        return ConnectTime;
    }
    
    /// Get the current connection time
    /// - Parameter time: <#time description#>
    static func setConnectTime(time: Date) {
        ConnectTime = time;
    }
}
