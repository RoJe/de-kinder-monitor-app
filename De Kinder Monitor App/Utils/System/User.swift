//
//  User.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/8/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import Foundation
import Alamofire


/// the permissions the user has struct
struct UserConnectedPermission: Equatable {
    var patientID: Int
    var permissions: [PermissionsData]
}


class User {
    
    /// The user his data
    private static var userData: UserData = UserData()
    
    /// The user his permissions
    private static var permissionData: PermissionsData = PermissionsData();
    
    /// Get the user permissions data
    static func GetPermissionData() -> PermissionsData {
        return permissionData;
    }
    
    /// Set the user permission data
    /// - Parameter permission: <#permission description#>
    static func setPermissionData(permission: PermissionsData) {
        permissionData = permission;
    }
    
    /// Get the data from the user
    static func GetData() -> UserData?{
        return User.userData
    }
    
    /// Set the data from teh user
    /// - Parameter user: <#user description#>
    static func SetData(user: UserData){
        User.userData = user
    }
    
    /// Get if the user is logged in
    static func GetLoginStatus() -> Bool{
        let defaults = UserDefaults.standard
        let authToken = defaults.string(forKey: "AuthToken")
        if authToken == nil || authToken == "" {
            return false
        }
        return true
    }
    
    /// Login the user
    /// - Parameter str: authentication token
    static func Login(str: String) {
        let defaults = UserDefaults.standard
        defaults.setValue(str, forKey: "AuthToken")
        defaults.synchronize()
    }
    
    /// alert the user about loggout
    static func UserLogout(){
        CustomAlert.Show(title: NSLocalizedString("Warning", comment: ""),
                         msg: NSLocalizedString("WarnLogout", comment: ""),
                         h1Text: NSLocalizedString("Close", comment: ""),
                         h2Text: NSLocalizedString("Logout", comment: ""),
                         h1: nil,
                         h2: {(alert: UIAlertAction!) in
                            DirectLogout();
                        })
    }
    
    
    /// Logout the user
    static func DirectLogout() {
        let defaults = UserDefaults.standard
        defaults.setValue(nil, forKey: "AuthToken")
        defaults.synchronize();
        userData = UserData();
        permissionData = PermissionsData();
        Sensor.destroy()
        Network.DisconnectSocket()
        Navigation.ReplaceRoot(strb: "LandingScreen", view: "LandingViewNavController")
    }
    
    
    /// Get the autentication token from the user
    static func GetAuthToken() -> String? {
        return UserDefaults.standard.string(forKey: "AuthToken")
    }
    
    /// Check if the user his name is valid
    /// - Parameter firstName:his first name
    static func CorrectFirstName(firstName: String) -> String? {
        if (firstName.isEmpty){
            return NSLocalizedString("ErrorNoName", comment: "");
        }
        
        if (firstName.count > 15){
            return NSLocalizedString("ErrorNameToLong", comment: "");
        }
        
        return nil
    }
    
    /// Check if he has a valid last name
    /// - Parameter lastName: user his last name
    static func CorrectLastName(lastName: String) -> String? {
        if (lastName.isEmpty){
            return NSLocalizedString("ErrorNoSurName", comment: "");
        }
        
        if (lastName.count > 15){
            return NSLocalizedString("ErrorSurNameToLong", comment: "");
        }
        
        return nil
    }
    
    
    /// Check if his day of birth is correct
    /// - Parameter birthday: user his birthday
    static func CorrectBirthDay(birthday: String) -> String? {
        if (!birthday.CorrectBirthday){
            return NSLocalizedString("ErrorBirthDate", comment: "");
        }
        
        return nil
    }
    
    /// Check if the user his email is correct
    /// - Parameter email: User his email
    static func CorrectEmail(email: String) ->  String?{

        if( !email.CorrectEmail ) {
            return  NSLocalizedString("ErrorEmail", comment: "");
        }
        
        return nil
    }
    
    /// Check if the user his username is correct
    /// - Parameter username: User his username
    static func CorrectUsername(username: String) ->  String?{

        if( username.count < 6 ) {
            return  NSLocalizedString("ErrorUserToLong", comment: "");
        }
        
        return nil
    }
    
    /// Check if the user his password is correct
    /// - Parameters:
    ///   - password: User his pasword
    ///   - password2: User his optional second password for validation purposes
    static func CorrectPassword(password: String, _ password2: String?) -> String?{
        if(password2 != nil && password != password2){
            return NSLocalizedString("PassError1", comment: "");
        }
        if (password.count < 8 ){
            return NSLocalizedString("PassError2", comment: "");
        }
        if ( !password.containsUppercase ){
            return NSLocalizedString("PassError3", comment: "");
        }
        if( !password.containsLowercase ){
            return NSLocalizedString("PassError4", comment: "");
        }
        if( !password.containsDigit ){
            return NSLocalizedString("PassError5", comment: "");
        }
        if( !password.CorrectPassword ) {
            return NSLocalizedString("PassError6", comment: "");
        }
        
        return nil
    }
    
    // MARK: - Login user data netwerk callers.
    
    /// Get user permission network call
    /// - Parameter group: group to connect to
    static func LoginSetUserPermissions(_ group: DispatchGroup?){
        Network.getUserPermissions(
            urlFail: {
        }, comp: { (json, error, response) in
            do {
                if response.response?.statusCode == 200 {
                    let permission = try JSONDecoder().decode(Array<PermissionsData>.self, from: Data(json!.utf8) )
                    permissionData = permission[0];
                    group?.leave()
                } else {
                    Network.netAlert();
                    group?.leave()
                }
            } catch{
                Network.netAlert();
                group?.leave()
            }
        }, fail: { error in
            //If the fail is called a 404 has occured, there is no permission data available, show empty homescreen.
            group?.leave()
        })
    }
    
    /// Set the user his data
    /// - Parameter group: group to connect to
    static func LoginSetUserData(_ group: DispatchGroup?) {
        Network.GetCurrentUser(
        urlFail: {
            Network.netAlert();
            }, comp: {(json, error, response) in
                do {
                    if response.response?.statusCode == 200 {
                        if let json = json {
                            let user = try JSONDecoder().decode(UserData.self, from: Data(json.utf8) )
                            User.SetData(user: user);
                        }
                    }else{
                        Network.netAlert();
                        User.DirectLogout()
                    }
                    group?.leave()
                }catch{
                    Network.netAlert();
                    group?.leave()
                    User.DirectLogout()
                }
            }, fail: { error in
                group?.leave()
                Network.netAlert();
        })
    }
}
