//
//  Styling.swift
//  De Kinder Monitor App
//
//  Created by Maikel Witlox on 06/01/2020.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit

/// Simple colors
class Colors {
    static var TransParentRed = UIColor.init(red: CGFloat(211/255.0), green: CGFloat(47/255.0), blue: CGFloat(47/255.0), alpha: CGFloat(0.3));
    static var Red = UIColor.init(red: CGFloat(211/255.0), green: CGFloat(47/255.0), blue: CGFloat(47/255.0), alpha: CGFloat(1));
    static var Green = UIColor.init(red: CGFloat(0), green: CGFloat(153/255.0), blue: CGFloat(0), alpha: CGFloat(1));
}
