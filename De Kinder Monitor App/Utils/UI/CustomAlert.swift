//
//  VieuwController.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 1/3/20.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import UIKit


/// Basic alert class
class CustomAlert {
    typealias handler = ((UIAlertAction) -> Void)? // Basic function structure for arguments
    
    
    /// Show custom simple alert
    /// - Parameters:
    ///   - title: The presented title
    ///   - msg: the message
    ///   - h1Text: the text in the first button
    ///   - h1: The function to complete
    static func Show(title: String, msg: String, h1Text: String, h1: handler){
        // create the alert
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)

        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: h1Text, style: UIAlertAction.Style.default, handler: h1))

        // show the alert
        Navigation.GetTopViewController()?.present(alert, animated: true, completion: nil)
    }
    
    /// Show custom simple alert
    /// - Parameters:
    ///   - title: the title
    ///   - msg: the message
    ///   - h1Text: button text
    ///   - h2Text: button text
    ///   - h1: function
    ///   - h2: function
    static func Show(title: String, msg: String, h1Text: String, h2Text: String, h1: handler, h2: handler){
        // create the alert
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)

        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: h1Text, style: UIAlertAction.Style.default, handler: h1))
        alert.addAction(UIAlertAction(title: h2Text, style: UIAlertAction.Style.cancel, handler: h2))

        // show the alert
        Navigation.GetTopViewController()?.present(alert, animated: true, completion: nil)
    }
    
    /// Show custom simple alert
    /// - Parameters:
    ///   - title: the title
    ///   - msg: the message
    ///   - h1Text: button text
    ///   - h2Text: button text
    ///   - h3Text: button text
    ///   - h1: function
    ///   - h2: function
    ///   - h3: function
    static func Show(title: String, msg: String, h1Text: String, h2Text: String, h3Text: String, h1: handler, h2: handler, h3: handler){
        // create the alert
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)

        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: h1Text, style: UIAlertAction.Style.default, handler: h1))
        alert.addAction(UIAlertAction(title: h3Text, style: UIAlertAction.Style.cancel, handler: h3))
        alert.addAction(UIAlertAction(title: h2Text, style: UIAlertAction.Style.destructive, handler: h2))

        // show the alert
        Navigation.GetTopViewController()?.present(alert, animated: true, completion: nil)
    }
}
