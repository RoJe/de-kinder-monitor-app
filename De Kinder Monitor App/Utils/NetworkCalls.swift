//
//  NetworkCalls.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 1/17/20.
//  Copyright © 2020 Robbert De jel. All rights reserved.
//

import Foundation
import Alamofire


struct UserData: Decodable, Equatable {
    var userID: Int?
    var username: String?
    var password: String?
    var firstName: String?
    var lastName: String?
    var phoneNumber: String?
    var email: String?
    var type: Int?
}

struct PatientData: Decodable, Equatable {
    var patientID: Int
    var firstname: String
    var lastname: String
    var birthday: String
}

struct PermissionsData: Decodable, Equatable {
    var permissionID: Int
    var userID: Int
    var patientID: Int
    var write: Bool
}

class NetworkCalls {
    
    /// Function to check if the user is still loged in.
    /// - Parameters:
    ///   - url: url to send to.
    ///   - urlFailure: action to execute if the url doesn't work
    ///   - completion: action to execute if the request was succesfull
    ///   - failure: action to execute if the request was a failure
    public static func IsUserValid(urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: "https://ehealthapi2r-fun.azurewebsites.net/api/Users/UserStillValid") else {
            urlFail?()
            return
        }
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    
    static func Register(user: UserData, urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: "https://ehealthapi2r-fun.azurewebsites.net/api/Users") else {
            urlFail?()
            return
        }
        
        Network.Post(url: urlToExecute, headers: nil, parameters: [
            "Userid": 0,
            "Username": user.username,
            "Password": user.password,
            "FirstName": user.firstName,
            "LastName": user.lastName,
            "PhoneNumber": user.phoneNumber,
            "Email": user.email
            ], completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    static func GetCurrentUser(urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: "https://ehealthapi2r-fun.azurewebsites.net/api/Users/me") else {
            urlFail?()
            return
        }
        
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    static func CreatePermission(userid: Int, urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: "https://ehealthapi2r-fun.azurewebsites.net/api/Users/me/Patients/Permissions") else {
            urlFail?()
            return
        }
        Network.Post(
            url: urlToExecute,
            headers:  [
                "Authorization" : "Bearer \(User.GetAuthToken() ?? "")",
                "Accept": "application/json"
            ],
            parameters: [
                "PermissionId": 0,
                "UserId": User.GetData()?.userID ?? 0,
                "PatientId": 7,
                "Write": true
            ],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
        })
    }
    
    static func GetUserConnectedPatients(urlFail: normalAction, comp: response, fail: errorAction){
        guard let urlToExecute = URL(string: "https://ehealthapi2r-fun.azurewebsites.net/api/Users/me/patients") else {
            urlFail?()
            return
        }
        Network.Get(
            url: urlToExecute,
            headers: ["Authorization" : "Bearer \(User.GetAuthToken() ?? "")"],
            completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
           })
    }
    
    static func GetUserPermissions(patients: [PatientData], urlFail: normalAction, comp: response, fail: errorAction) {
        for patients in patients {
            guard let urlToExecute = URL(string: "https://ehealthapi2r-fun.azurewebsites.net/api/Users/me/patients/\(patients.patientID)/permissions") else {
                urlFail?()
                return
            }
            Network.Get(
                url: urlToExecute,
                headers: [
                    "Authorization" : "Bearer \(User.GetAuthToken() ?? "")",
                    "Accept": "application/json"
                ],completion: {(json, error, response) in
                    if let error = error {
                        fail?(error)
                        return
                    }
                    comp?(json, error, response)
            })
        }
    }
    
    static func RemovePermission(patientID: Int, urlFail: normalAction, comp: response, fail: errorAction) {
            guard let urlToExecute = URL(string: "https://ehealthapi2r-fun.azurewebsites.net/api/Users/me/Patients/\(patientID)/Permissions") else {
                urlFail?()
                return
            }
            Network.Delete(
                url: urlToExecute,
                headers:  [
                    "Authorization" : "Bearer \(User.GetAuthToken() ?? "")",
                    "Accept": "application/json"
            ], completion: {(json, error, response) in
                if let error = error {
                    fail?(error)
                    return
                }
                comp?(json, error, response)
            })
    }
    
    
    static func GetConnectedPatients(permissions: [PermissionsData], urlFail: normalAction, comp: response, fail: errorAction) {
        for  permission in permissions{
            guard let urlToExecute = URL(string: "https://ehealthapi2r-fun.azurewebsites.net/api/Users/GetUserById/\(permission.userID)") else {
                urlFail?()
                return
            }
            Network.Get(
                url: urlToExecute,
                headers: [
                    "Authorization" : "Bearer \(User.GetAuthToken() ?? "")",
                    "Accept": "application/json"
                ],completion: {(json, error, response) in
                    if let error = error {
                        fail?(error)
                        return
                    }
                    comp?(json, error, response)
                })
            }
    }
}
