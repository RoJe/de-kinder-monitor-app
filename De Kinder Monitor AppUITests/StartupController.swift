//
//  StartupController.swift
//  De Kinder Monitor App
//
//  Created by Robbert De jel on 12/8/19.
//  Copyright © 2019 Robbert De jel. All rights reserved.
//

import UIKit

class StartupController : UIViewController{
    // MARK: - Properties
    var delegate: StartupControllerDelegate?
    private var user : User?
    
    private let networkHandler = Network()
    
    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.initialize()
        self.Action()
    }
    // MARK: - Handlers
    
    private func initialize(){
        self.user = User()
    }
    
    private func Action(){
        if (!self.user!.getUserLoginStatus()){
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginOrRegister")

            self.navigationController?.pushViewController(secondViewController!, animated: true)
            print("kjlajshdlkajshd")
        }
    }
}
